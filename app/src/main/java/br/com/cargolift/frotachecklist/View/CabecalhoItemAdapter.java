package br.com.cargolift.frotachecklist.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.cargolift.frotachecklist.R;

public class CabecalhoItemAdapter extends ArrayAdapter<DataModel> {

    private ArrayList<DataModel> dataSet;
    Context mContext;


    private static class ViewHolder {
        TextView txtBem;
        TextView txtData;
        TextView txtHora;
    }

    public CabecalhoItemAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.item_cabecalho, data);
        this.dataSet = data;
        this.mContext=context;

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_cabecalho, parent, false);
            viewHolder.txtBem = (TextView) convertView.findViewById(R.id.TVTitle);
            viewHolder.txtData = (TextView) convertView.findViewById(R.id.TVData);
            viewHolder.txtHora = (TextView) convertView.findViewById(R.id.TVHora);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        result.setVisibility(View.VISIBLE);
        lastPosition = position;

        viewHolder.txtBem.setText(dataModel.getBem());
        viewHolder.txtData.setText(dataModel.getData());
        viewHolder.txtHora.setText(dataModel.getHora());
        // Return the completed view to render on screen
        return convertView;
    }
}