package br.com.cargolift.frotachecklist.View;

public class DataModel {
    String Bem;
    String Data;
    String Hora;

    public DataModel(String Bem, String Data, String Hora) {
        this.Bem=Bem;
        this.Data=Data;
        this.Hora=Hora;

    }

    public String getBem() {
        return Bem;
    }

    public String getData() {
        return Data;
    }

    public String getHora() {
        return Hora;
    }

}