package br.com.cargolift.frotachecklist.View;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rm.rmswitch.RMSwitch;
import com.rm.rmswitch.RMTristateSwitch;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;
import java.util.Vector;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;
import it.neokree.materialtabs.MaterialTabHost;
import ru.kolotnev.formattedittext.MaskedEditText;

import static java.lang.Boolean.FALSE;

@TargetApi(Build.VERSION_CODES.N)
public class InicioChecklist extends AppCompatActivity {
    static final int COL_BEM_ID = 0;
    static final int COL_BEM_FK_CODFAM = 1;
    static final int COL_BEM_FK_TIPMOD = 2;
    static final int COL_BEM_COLUMN_CODBEM = 3;
    static final int COL_BEM_COLUMN_NOME = 4;
    static final int COL_BEM_COLUMN_PLACA = 5;
    static final int COL_BEM_COLUMN_FABRICA = 6;
    static final int COL_FAMILIA_ID = 0;
    static final int COL_FAMILIA_COLUMN_CODFAM = 1;
    static final int COL_FAMILIA_COLUMN_NOME = 2;
    static final int COL_MOTORISTA_ID = 0;
    static final int COL_MOTORISTA_COLUMN_NOME = 1;
    static final int COL_MOTORISTA_COLUMN_CPF = 2;

    static final int COL_MODCHECK_ID = 0;
    static final int COL_MODCHECK_COLUMN_CODFAM = 1;
    static final int COL_MODCHECK_COLUMN_TIPMOD= 2;
    static final int COL_MODCHECK_COLUMN_SEQFAM = 3;
    static final int COL_MODCHECK_COLUMN_NOMSEQ= 4;
    static final int COL_MODCHECK_COLUMN_NUMSEQ = 5;
    static final int COL_MODCHECK_COLUMN_TPCHKL= 6;
    static final int COL_MODCHECK_COLUMN_MDCHKL = 7;

    private static final String[] USERLIST_COLUMNS = {
            checklistContract.checklistUserEntry.TABLE_NAME +"."+
                    checklistContract.checklistUserEntry._ID,
            checklistContract.checklistUserEntry.USERNAME,
            checklistContract.checklistUserEntry.PASSWORD,
            checklistContract.checklistUserEntry.EMAIL
    };
    public static final String[] ETAPA_COLUMNS = {
            checklistContract.EtapaEntry.TABLE_NAME +"."+
                    checklistContract.EtapaEntry._ID,
            checklistContract.EtapaEntry.COLUMN_ETAPA,
            checklistContract.EtapaEntry.COLUMN_DESCRI
    };
    private static final String[] ItemChecklist_COLUMNS = {
            checklistContract.ItemChecklistEntry.TABLE_NAME +"."+
                    checklistContract.ItemChecklistEntry._ID,
            checklistContract.ItemChecklistEntry.COLUMN_Etapa,
            checklistContract.ItemChecklistEntry.COLUMN_Desc,
            checklistContract.ItemChecklistEntry.COLUMN_Critic,
            checklistContract.ItemChecklistEntry.COLUMN_Img,
            checklistContract.ItemChecklistEntry.COLUMN_TextoComp,
            checklistContract.ItemChecklistEntry.COLUMN_data,
            checklistContract.ItemChecklistEntry.COLUMN_hora,
            checklistContract.ItemChecklistEntry.COLUMN_status
    };
    static final int COL_ETAPA_ID = 0;
    static final int COL_ETAPA_CODETAPA = 1;
    static final int COL_ETAPA_DESCETAPA = 2;

    static final int COL_USERLIST_ID = 0;
    static final int COL_USERLIST_USERNAME = 1;
    static final int COL_USERLIST_PASSWORD = 2;
    static final int COL_USERLIST_USERKEY = 3;

    static final int COL_CHECKLIST_ID = 0;
    static final int COL_CHECKLIST_ETAPA = 1;
    static final int COL_CHECKLIST_SERVIC = 2;
    static final int COL_CHECKLIST_TIPMOD = 3;
    static final int COL_CHECKLIST_ALTA = 4;
    static final int COL_CHECKLIST_MEDIA = 5;
    static final int COL_CHECKLIST_BEIXA = 6;
    static final int COL_CHECKLIST_SEQFAM = 7;

    private static final String[] BEM_COLUMNS = {

            checklistContract.BemEntry._ID,
            checklistContract.BemEntry.FK_CODFAM,
            checklistContract.BemEntry.FK_TIPMOD,
            checklistContract.BemEntry.COLUMN_CODBEM,
            checklistContract.BemEntry.COLUMN_NOME,
            checklistContract.BemEntry.COLUMN_PLACA,
            checklistContract.BemEntry.COLUMN_FABRICA
    };
    private static final String[] FAMILIA_COLUMNS = {

            checklistContract.FamiliaEntry._ID,
            checklistContract.FamiliaEntry.COLUMN_CODFAM,
            checklistContract.FamiliaEntry.COLUMN_NOME
    };
    private static final String[] FUNCIONARIO_COLUMNS = {

            checklistContract.FuncionarioEntry._ID,
            checklistContract.FuncionarioEntry.COLUMN_NOME,
            checklistContract.FuncionarioEntry.COLUMN_EMAIL
    };
    private static final String[] MOTORISTA_COLUMNS = {

            checklistContract.MotoristaEntry._ID,
            checklistContract.MotoristaEntry.COLUMN_NOME,
            checklistContract.MotoristaEntry.COLUMN_CGC
    };
    private static final String[] Checklist_COLUMNS = {
            checklistContract.ChecklistkEntry.TABLE_NAME +"."+
                    checklistContract.ChecklistkEntry._ID,
            checklistContract.ChecklistkEntry.FK_ETAPA,
            checklistContract.ChecklistkEntry.FK_SERVIC,
            checklistContract.ChecklistkEntry.FK_TIPMOD,
            checklistContract.ChecklistkEntry.COLUMN_ALTA,
            checklistContract.ChecklistkEntry.COLUMN_MEDIA,
            checklistContract.ChecklistkEntry.COLUMN_BAIXA,
            checklistContract.ChecklistkEntry.COLUMN_SEQFAM

    };
    private static final String[] Mod_Checklist_COLUMNS = {
            checklistContract.ModCheckEntry.TABLE_NAME +"."+
                    checklistContract.ModCheckEntry._ID,
            checklistContract.ModCheckEntry.FK_CODFAM,
            checklistContract.ModCheckEntry.FK_TIPMOD,
            checklistContract.ModCheckEntry.COLUMN_SEQFAM,
            checklistContract.ModCheckEntry.COLUMN_NOMSEQ,
            checklistContract.ModCheckEntry.COLUMN_NUMSEQ,
            checklistContract.ModCheckEntry.COLUMN_TPCHKL,
            checklistContract.ModCheckEntry.COLUMN_MDCHKL
    };
    RMSwitch mSwitch;
    RMSwitch mSwitch2;
    RMTristateSwitch mTristateSwitch;
    String mCPF = "";
    String mFrota;
    String mPlaca;
    String mModelo;
    String mFamilia;
    String mTPCHKL = "S";
    String mMODCHKL = "C";
    String SEQFAM;
    MaterialTabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_checklist);


        TextView TV_NomeFrota = (TextView) findViewById(R.id.NomeFrota);
        TextView TV_PlacaVeiculo = (TextView) findViewById(R.id.PlacaVeiculo);
        TextView TV_NomeFamilia = (TextView) findViewById(R.id.TipoVeiculo);
        TextView TV_NomeModelo = (TextView) findViewById(R.id.ModeloVeiculo);
        ImageView IV_LogoFabrica = (ImageView) findViewById(R.id.MarcaVeiculo);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final Button button = (Button) findViewById(R.id.iniciachecklist);
        final MaskedEditText CPFMot = (MaskedEditText) findViewById(R.id.CPFMot);


        String BuscarPlaca;

        if(savedInstanceState == null){
            Bundle extras = getIntent().getExtras();
            if(extras == null){
                BuscarPlaca = null;
            }else {
                BuscarPlaca = extras.getString("Placa");
            }
        }else{
            BuscarPlaca = (String) savedInstanceState.getSerializable("Placa");
        }

        String[] mSELECT_BEM_ARGUMENTS = {""};
        String[] mSELECT_FAM_ARGUMENTS = {""};
        final Uri BuscaPlaca = checklistContract.BemEntry.buildBemUri();
        final Uri BuscaFamilia = checklistContract.FamiliaEntry.buildFamiliaUri();

        String selectBemWithCODBEM;
        String selectFAMWithCODFAM;
        if (TextUtils.isEmpty(BuscarPlaca)) {
            selectBemWithCODBEM = null;
            mSELECT_BEM_ARGUMENTS[0] = "";

        } else {
            selectBemWithCODBEM = checklistContract.BemEntry.COLUMN_CODBEM + " = ?";

            mSELECT_BEM_ARGUMENTS[0] = BuscarPlaca;
        }
        final Cursor mCursorBem = getContentResolver().query(
                BuscaPlaca, BEM_COLUMNS, selectBemWithCODBEM, mSELECT_BEM_ARGUMENTS, "_id ASC");

        mCursorBem.moveToFirst();

        if (TextUtils.isEmpty(mCursorBem.getString(COL_BEM_FK_CODFAM))) {
            selectFAMWithCODFAM = null;
            mSELECT_FAM_ARGUMENTS[0] = "";

        } else {
            selectFAMWithCODFAM = checklistContract.FamiliaEntry.COLUMN_CODFAM + " = ?";

            mSELECT_FAM_ARGUMENTS[0] = mCursorBem.getString(COL_BEM_FK_CODFAM);
        }

        Cursor mCursorFamilia = getContentResolver().query(
                BuscaFamilia, FAMILIA_COLUMNS, selectFAMWithCODFAM, mSELECT_FAM_ARGUMENTS, "_id ASC");

        mCursorFamilia.moveToFirst();

        String CodBem = mCursorBem.getString(COL_BEM_COLUMN_CODBEM);
        mFrota = CodBem;
        String Fabrica = mCursorBem.getString(COL_BEM_COLUMN_FABRICA);
        String FabricaFormat = Fabrica.substring(Fabrica.length()-2);

        if (Objects.equals(FabricaFormat, "03")){
            IV_LogoFabrica.setImageResource(R.drawable.hyundailogo);
        }else if (Objects.equals(FabricaFormat, "04")){
            IV_LogoFabrica.setImageResource(R.drawable.ivecologo);
        }else if (Objects.equals(FabricaFormat, "06")){
            IV_LogoFabrica.setImageResource(R.drawable.mercedeslogo);
        }else if (Objects.equals(FabricaFormat, "09")){
            IV_LogoFabrica.setImageResource(R.drawable.scanialogo);
        }else if (Objects.equals(FabricaFormat, "10")){
            IV_LogoFabrica.setImageResource(R.drawable.volvologo);
        }else if (Objects.equals(FabricaFormat, "11")){
            IV_LogoFabrica.setImageResource(R.drawable.iinternationallogo);
        }else if (Objects.equals(FabricaFormat, "12")){
            IV_LogoFabrica.setImageResource(R.drawable.chevroletlogo);
        }else if (Objects.equals(FabricaFormat, "13")){
            IV_LogoFabrica.setImageResource(R.drawable.toyotalogo);
        }else if (Objects.equals(FabricaFormat, "17")){
            IV_LogoFabrica.setImageResource(R.drawable.volkswagenlogo);
        }else if (Objects.equals(FabricaFormat, "20")){
            IV_LogoFabrica.setImageResource(R.drawable.fordlogo);
        }else if (Objects.equals(FabricaFormat, "21")){
            IV_LogoFabrica.setImageResource(R.drawable.fcalogo);
        }else if (Objects.equals(FabricaFormat, "38")){
            IV_LogoFabrica.setImageResource(R.drawable.manlogo);
        }else if (Objects.equals(FabricaFormat, "39")){
            IV_LogoFabrica.setImageResource(R.drawable.daflogo);
        }

        String CodModelo = mCursorBem.getString(COL_BEM_FK_TIPMOD);
        mModelo = CodModelo;
        String CodFamilia = mCursorFamilia.getString(COL_FAMILIA_COLUMN_NOME);
        mFamilia = mCursorFamilia.getString(COL_FAMILIA_COLUMN_CODFAM);
        String PlacaVeiculo = mCursorBem.getString(COL_BEM_COLUMN_PLACA);
        mPlaca = PlacaVeiculo;




        TV_PlacaVeiculo.setText(PlacaVeiculo);
        TV_NomeFrota.setText(CodBem);
        TV_NomeFamilia.setText(CodFamilia);
        TV_NomeModelo.setText(CodModelo);

//
//        String teste = mCursor.getString(COL_BEM_ID) +
//                " - " + mCursor.getString(COL_BEM_FK_CODFAM) +
//                " - " + mCursor.getString(COL_BEM_FK_TIPMOD) +
//                " - " + mCursor.getString(COL_BEM_COLUMN_CODBEM)+
//                " - " + mCursor.getString(COL_BEM_COLUMN_NOME)+
//                " - " + mCursor.getString(COL_BEM_COLUMN_PLACA)+
//                " - " + mCursor.getString(COL_BEM_COLUMN_FABRICA);

        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("LoggedUser", MODE_PRIVATE);
        final String mUser = pref2.getString("User", null);

        final String mMail = pref2.getString("Mail", null);

        String[] mSELECT_FUNC_ARGUMENTS = {""};
        mSELECT_FUNC_ARGUMENTS[0] = mMail;
        final Uri BuscaFunc = checklistContract.FuncionarioEntry.buildFuncionarioUri();
        Cursor mCursorFunc = getContentResolver().query(
                BuscaFunc, FUNCIONARIO_COLUMNS, checklistContract.FuncionarioEntry.COLUMN_EMAIL + " = ?", mSELECT_FUNC_ARGUMENTS, "_id ASC");

        int exfunc = mCursorFunc.getCount();

        mSwitch = (RMSwitch) findViewById(R.id.your_id);
        mSwitch2 = (RMSwitch) findViewById(R.id.your_id2);

        if (exfunc > 0 ){
            mSwitch2.setChecked(true);
            mSwitch2.setEnabled(true);
            mSwitch2.setForceAspectRatio(false);
        }else {
            mMODCHKL = "S";
            mSwitch2.setChecked(false);
            mSwitch2.setEnabled(false);
            mSwitch2.setForceAspectRatio(false);
            TextView tpcomp = (TextView) findViewById(R.id.tpComp);
            tpcomp.setTextColor(getResources().getColor(R.color.grey));
        }
        // Setup the switch
        mSwitch.setChecked(true);
        mSwitch.setEnabled(true);
        mSwitch.setForceAspectRatio(false);


        // You can choose if use drawable or drawable resource
        //mSwitch.setSwitchToggleCheckedDrawableRes(android.R.drawable.ic_media_next);
        mSwitch.setSwitchToggleCheckedDrawable(ContextCompat.getDrawable(this, R.drawable.ic_uparrow));

        //mSwitch.setSwitchToggleNotCheckedDrawableRes(android.R.drawable.ic_media_previous);
        mSwitch.setSwitchToggleNotCheckedDrawable(ContextCompat.getDrawable(this, R.drawable.ic_downarrow));

        mSwitch2.setSwitchToggleCheckedDrawable(ContextCompat.getDrawable(this, R.drawable.ic_complete));

        //mSwitch.setSwitchToggleNotCheckedDrawableRes(android.R.drawable.ic_media_previous);
        mSwitch2.setSwitchToggleNotCheckedDrawable(ContextCompat.getDrawable(this, R.drawable.ic_notcomplete));



        // Add a Switch state observer
        mSwitch.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if(isChecked){
                    mTPCHKL = "S";
                }else {
                    mTPCHKL = "E";
                }
            }
        });
        mSwitch2.addSwitchObserver(new RMSwitch.RMSwitchObserver() {
            @Override
            public void onCheckStateChange(RMSwitch switchView, boolean isChecked) {
                if(isChecked){
                    mMODCHKL = "C";
                }else {
                    mMODCHKL = "S";
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String VALIDACPF = String.valueOf(CPFMot.getText(true));

                if (!Objects.equals(VALIDACPF, "")){
                    final Uri BuscaCPF = checklistContract.MotoristaEntry.buildMotoristaUri();

                    String[] mSELECT_MOTORISTA_ARGUMENTS = {""};

                    mSELECT_MOTORISTA_ARGUMENTS[0] = VALIDACPF;

                    String selectMOtWithCPF = checklistContract.MotoristaEntry.COLUMN_CGC + " = ?";


                    Cursor mCursorCPF = getContentResolver().query(
                            BuscaCPF, MOTORISTA_COLUMNS, selectMOtWithCPF, mSELECT_MOTORISTA_ARGUMENTS, "_id ASC");

                    int resultquery = mCursorCPF.getCount();

                    if (resultquery > 0){
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MontagemChecklist", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();



                        editor.putString("mFrota", mFrota);
                        editor.putString("mModelo", mModelo);
                        editor.putString("mFamilia", mFamilia);
                        editor.putString("mPlaca", mPlaca);
                        editor.putString("mTPCHKL", mTPCHKL);
                        editor.putString("mMODCHKL", mMODCHKL);

                        editor.apply();

                        mCPF = VALIDACPF;

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
                        SimpleDateFormat hora = new SimpleDateFormat("kk:mm");
                        final String dataformatada = data.format(c.getTime());
                        final String horafortmatada = hora.format(c.getTime());

                        SharedPreferences SaveItemDate = getApplicationContext().getSharedPreferences("InicioChecklist", MODE_PRIVATE);
                        SharedPreferences.Editor editorItemDate = SaveItemDate.edit();

                        editorItemDate.putString("Data", dataformatada);

                        editorItemDate.putString("Hora", horafortmatada);

                        editorItemDate.apply();


                        ContentValues CabecalhoChecklist = new ContentValues();

                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_Bem, mFrota);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_Placa, mPlaca);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_Familia, mFamilia);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_Data, dataformatada);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_Hora, horafortmatada);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_TpCheck, mTPCHKL);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_ModCheck, mMODCHKL);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_SitVeic, "");
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_CPFMot, mCPF);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_AssMot, "");
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_USER, mUser);
                        CabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_TIPMOD, mModelo);

                        Vector<ContentValues> cVVectorCabecalhoCheck = new Vector<ContentValues>();

                        String SelectEtapaType;

                        SelectEtapaType = checklistContract.ModCheckEntry.FK_CODFAM + " = ?" + " AND " + checklistContract.ModCheckEntry.COLUMN_MDCHKL + " = ?";

                        String[] mSELECT_TTD_ARGUMENTS = {"",""};
                        mSELECT_TTD_ARGUMENTS[0] = mFamilia;
                        mSELECT_TTD_ARGUMENTS[1] = mMODCHKL;

                        final Uri BuscaEtapasModChecklist = checklistContract.ModCheckEntry.buildModCheckUri();

                        Cursor mCursorBCheckMod = getApplicationContext().getContentResolver().query(
                                BuscaEtapasModChecklist, Mod_Checklist_COLUMNS, SelectEtapaType, mSELECT_TTD_ARGUMENTS, "_ID ASC");

                        int getCursorCounter = mCursorBCheckMod.getCount();
                        if (getCursorCounter > 0 ){
                            mCursorBCheckMod.moveToFirst();
                            SEQFAM = mCursorBCheckMod.getString(COL_MODCHECK_COLUMN_SEQFAM);
                        }else{
                            SEQFAM = "001";
                        }
                        String SelectEtapaWithFamiliaAndModeloFromChecklist;

                        SelectEtapaWithFamiliaAndModeloFromChecklist = checklistContract.ChecklistkEntry.FK_CODFAM + " = ?" + " AND " + checklistContract.ChecklistkEntry.COLUMN_SEQFAM + " = ?";

                        String[] mSELECT_TTE_ARGUMENTS = {"",""};
                        mSELECT_TTE_ARGUMENTS[0] = mFamilia;
                        mSELECT_TTE_ARGUMENTS[1] = SEQFAM;

                        final Uri BuscaEtapasChecklist = checklistContract.ChecklistkEntry.buildChecklistUri();

                           Cursor mCursorBem = getApplicationContext().getContentResolver().query(
                                BuscaEtapasChecklist, Checklist_COLUMNS, SelectEtapaWithFamiliaAndModeloFromChecklist, mSELECT_TTE_ARGUMENTS, "_ID ASC");

                        int getCursor = mCursorBem.getCount();

                        if (getCursor > 0) {
                            cVVectorCabecalhoCheck.add(CabecalhoChecklist);

                            int inserted = 0;
                            // add to database
                            if (cVVectorCabecalhoCheck.size() > 0) {
                                ContentValues[] cvArrayCabecalhoCheck = new ContentValues[cVVectorCabecalhoCheck.size()];
                                cVVectorCabecalhoCheck.toArray(cvArrayCabecalhoCheck);

                                //getApplicationContext().getContentResolver().delete(checklistContract.ChecklistkEntry.CONTENT_URI,
                                //checklistContract.ChecklistkEntry._ID + " > ?", new String[]{"0"});

                                getApplicationContext().getContentResolver().bulkInsert(checklistContract.CabecalhoChecklistEntry.CONTENT_URI, cvArrayCabecalhoCheck);

                            }








                            //--------------------------------------------------------------------//



                            if (mFrota == null) {
                                SelectEtapaWithFamiliaAndModeloFromChecklist = null;
                                mSELECT_TTE_ARGUMENTS[0] = "";
                                mSELECT_TTE_ARGUMENTS[1] = "";

                            } else {
                                SelectEtapaWithFamiliaAndModeloFromChecklist = checklistContract.ChecklistkEntry.FK_CODFAM + " = ?" + " AND " + checklistContract.ChecklistkEntry.COLUMN_SEQFAM + " = ?";
                                mSELECT_TTE_ARGUMENTS[0] = mFamilia;
                                mSELECT_TTE_ARGUMENTS[1] = SEQFAM;
                            }


                            Cursor mCursorEtapas = getApplicationContext().getContentResolver().query(
                                    BuscaEtapasChecklist, Checklist_COLUMNS, SelectEtapaWithFamiliaAndModeloFromChecklist, mSELECT_TTE_ARGUMENTS, "_ID ASC");

                            mCursorEtapas.moveToFirst();



                            String[] mSELECT_Etapa_ARGUMENTS = {""};
                            String SelectEtapaWithCod = null;


                            for (int i = 0; i < mCursorEtapas.getCount(); i++) {
                                SelectEtapaWithCod = checklistContract.EtapaEntry.COLUMN_ETAPA + " = ?";
                                mSELECT_Etapa_ARGUMENTS[0] = mCursorEtapas.getString(COL_CHECKLIST_ETAPA);



                                Uri weatherForLocationUri = checklistContract.EtapaEntry.buildEtapaUri();

                                Cursor cur = getApplicationContext().getContentResolver().query(weatherForLocationUri, ETAPA_COLUMNS, SelectEtapaWithCod, mSELECT_Etapa_ARGUMENTS, "_id ASC");
                                cur.moveToFirst();
                                String CodeEtapa = cur.getString(COL_ETAPA_CODETAPA);
                                String DescEtapa = cur.getString(COL_ETAPA_DESCETAPA);

                                ContentValues ItemChecklist = new ContentValues();

                                ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_Etapa, CodeEtapa);
                                ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_Desc, DescEtapa);
                                ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_data, dataformatada);
                                ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_hora, horafortmatada);

                                Vector<ContentValues> cVVectorItemCheck = new Vector<ContentValues>();

                                cVVectorItemCheck.add(ItemChecklist);

                                // add to database
                                if (cVVectorItemCheck.size() > 0) {
                                    ContentValues[] cvArrayItemCheck = new ContentValues[cVVectorItemCheck.size()];
                                    cVVectorItemCheck.toArray(cvArrayItemCheck);

                                    getApplicationContext().getContentResolver().bulkInsert(checklistContract.ItemChecklistEntry.CONTENT_URI, cvArrayItemCheck);

                                }
                                mCursorEtapas.moveToNext();
                            }

                            Intent intent = new Intent(InicioChecklist.this, ListaChecklist.class);
                            startActivity(intent);
                        }else{
                            new MaterialDialog.Builder(InicioChecklist.this)
                                    .title(R.string.invalid_Checklist)
                                    .content(R.string.Dialog_Five_Text)
                                    .positiveText(R.string.Dialog_OK)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                            Intent intent = new Intent(getApplicationContext(), Checklist.class);
                                            startActivity(intent);

                                        }
                                    })
                                    .cancelable(FALSE)
                                    .show();
                        }


                    }else{
                        new MaterialDialog.Builder(InicioChecklist.this)
                                .title(R.string.driver_not_found)
                                .content(R.string.Dialog_Six_Text)
                                .positiveText(R.string.back)
                                .negativeText(R.string.TextView_atualizar)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        dialog.cancel();

                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Intent intent = new Intent(getApplicationContext(), Settings.class);
                                        startActivity(intent);
                                    }
                                })
                                .show();
                    }

                }else{
                    new MaterialDialog.Builder(InicioChecklist.this)
                            .title(R.string.driver_not_found)
                            .content(R.string.Dialog_Six_Text)
                            .positiveText(R.string.back)
                            .negativeText(R.string.TextView_atualizar)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                    dialog.cancel();

                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    Intent intent = new Intent(getApplicationContext(), Settings.class);
                                    startActivity(intent);
                                }
                            })
                            .show();
                }
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                new MaterialDialog.Builder(InicioChecklist.this)
                        .title(R.string.attention)
                        .content(R.string.backtomenu)
                        .positiveText(R.string.back)
                        .negativeText(R.string.cancelbutton)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                Intent intent = new Intent(getApplicationContext(), Checklist.class);
                                startActivity(intent);

                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(InicioChecklist.this)
                .title(R.string.attention)
                .content(R.string.backtomenu)
                .positiveText(R.string.back)
                .negativeText(R.string.cancelbutton)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        Intent intent = new Intent(getApplicationContext(), Checklist.class);
                        startActivity(intent);

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.cancel();
                    }
                })
                .show();
    }
}