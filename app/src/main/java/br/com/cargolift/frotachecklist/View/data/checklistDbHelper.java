/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.cargolift.frotachecklist.View.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.cargolift.frotachecklist.View.Checklist;
import br.com.cargolift.frotachecklist.View.data.checklistContract.checklistUserEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.MotoristaEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.BemEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.ModCheckEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.ChecklistkEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.ModeloEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.EtapaEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.ServicoEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.FamiliaEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.CabecalhoChecklistEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.ItemChecklistEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.JsonEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.FuncionarioEntry;
import br.com.cargolift.frotachecklist.View.data.checklistContract.LogEntry;
/**
 * Manages a local database for weather data.
 */
public class checklistDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;

    static final String DATABASE_NAME = "checklist.db";

    public checklistDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_USERLIST_TABLE = "CREATE TABLE " + checklistUserEntry.TABLE_NAME + " (" +
                checklistUserEntry._ID + " INTEGER PRIMARY KEY," +
                checklistUserEntry.USERNAME + " TEXT NOT NULL, " +
                checklistUserEntry.PASSWORD + " TEXT NOT NULL, " +
                checklistUserEntry.EMAIL + " TEXT NOT NULL, " +
                checklistUserEntry.NOME + " TEXT NOT NULL " +
                " );";
        final String SQL_CREATE_MOTORISTA_DA4_TABLE = "CREATE TABLE " + MotoristaEntry.TABLE_NAME + " (" +
                MotoristaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                MotoristaEntry.COLUMN_NOME + " TEXT NOT NULL, " +
                MotoristaEntry.COLUMN_CGC + " TEXT NOT NULL " +
                " );";
        final String SQL_CREATE_MODELO_TQR_TABLE = "CREATE TABLE " + ModeloEntry.TABLE_NAME + " (" +
                ModeloEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ModeloEntry.COLUMN_TIPMOD + " TEXT NOT NULL, " +
                ModeloEntry.COLUMN_DESMOD + " TEXT NOT NULL " +
                " );";
        final String SQL_CREATE_ETAPA_TPA_TABLE = "CREATE TABLE " + EtapaEntry.TABLE_NAME + " (" +
                EtapaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                EtapaEntry.COLUMN_ETAPA + " TEXT NOT NULL, " +
                EtapaEntry.COLUMN_DESCRI + " TEXT NOT NULL " +
                " );";
        final String SQL_CREATE_SERVICO_ST4_TABLE = "CREATE TABLE " + ServicoEntry.TABLE_NAME + " (" +
                ServicoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                ServicoEntry.COLUMN_SERVIC + " TEXT NOT NULL, " +
                ServicoEntry.COLUMN_NOME + " TEXT NOT NULL " +
                " );";
        final String SQL_CREATE_FAMILIA_ST6_TABLE = "CREATE TABLE " + FamiliaEntry.TABLE_NAME + " (" +
                FamiliaEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                FamiliaEntry.COLUMN_CODFAM + " TEXT NOT NULL, " +
                FamiliaEntry.COLUMN_NOME + " TEXT NOT NULL " +
                " );";

        final String SQL_CREATE_BEM_ST9_TABLE = "CREATE TABLE " + BemEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                BemEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                BemEntry.FK_CODFAM + " TEXT NOT NULL, " +
                BemEntry.FK_TIPMOD + " TEXT NOT NULL, " +
                BemEntry.COLUMN_CODBEM + " TEXT NOT NULL, " +
                BemEntry.COLUMN_NOME + " TEXT NOT NULL, " +
                BemEntry.COLUMN_PLACA + " TEXT NOT NULL, " +
                BemEntry.COLUMN_SITBEM + " TEXT NOT NULL, " +
                BemEntry.COLUMN_FABRICA + " TEXT NOT NULL," +

                " FOREIGN KEY (" + BemEntry.FK_CODFAM + ") REFERENCES " +
                FamiliaEntry.TABLE_NAME + " (" + FamiliaEntry.COLUMN_CODFAM + "), " +

                " FOREIGN KEY (" + BemEntry.FK_TIPMOD + ") REFERENCES " +
                ModeloEntry.TABLE_NAME + " (" + ModeloEntry.COLUMN_TIPMOD + ") " +
                " );";

        final String SQL_CREATE_MODCHECK_TTD_TABLE = "CREATE TABLE " + ModCheckEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                ModCheckEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                ModCheckEntry.FK_CODFAM + " TEXT NOT NULL, " +
                ModCheckEntry.FK_TIPMOD + " TEXT NOT NULL, " +
                ModCheckEntry.COLUMN_SEQFAM + " TEXT NOT NULL, " +
                ModCheckEntry.COLUMN_NOMSEQ + " TEXT NOT NULL, " +
                ModCheckEntry.COLUMN_NUMSEQ + " TEXT NOT NULL, " +
                ModCheckEntry.COLUMN_TPCHKL + " TEXT NOT NULL," +
                ModCheckEntry.COLUMN_MDCHKL + " TEXT NOT NULL," +

                " FOREIGN KEY (" + ModCheckEntry.FK_CODFAM + ") REFERENCES " +
                FamiliaEntry.TABLE_NAME + " (" + FamiliaEntry.COLUMN_CODFAM + "), " +

                " FOREIGN KEY (" + ModCheckEntry.FK_TIPMOD + ") REFERENCES " +
                ModeloEntry.TABLE_NAME + " (" + ModeloEntry.COLUMN_TIPMOD + ") " +
                " );";
        final String SQL_CREATE_CHECKLIST_TTE_TABLE = "CREATE TABLE " + ChecklistkEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                ChecklistkEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                ChecklistkEntry.FK_CODFAM + " TEXT NOT NULL, " +
                ChecklistkEntry.FK_ETAPA + " TEXT NOT NULL, " +
                ChecklistkEntry.FK_SERVIC + " TEXT NOT NULL, " +
                ChecklistkEntry.FK_TIPMOD + " TEXT NOT NULL, " +
                ChecklistkEntry.COLUMN_ALTA + " TEXT NOT NULL, " +
                ChecklistkEntry.COLUMN_MEDIA + " TEXT NOT NULL, " +
                ChecklistkEntry.COLUMN_BAIXA + " TEXT NOT NULL, " +
                ChecklistkEntry.COLUMN_SEQFAM + " TEXT NOT NULL," +

                " FOREIGN KEY (" + ChecklistkEntry.FK_CODFAM + ") REFERENCES " +
                FamiliaEntry.TABLE_NAME + " (" + FamiliaEntry.COLUMN_CODFAM + "), " +

                " FOREIGN KEY (" + ChecklistkEntry.FK_ETAPA + ") REFERENCES " +
                EtapaEntry.TABLE_NAME + " (" + EtapaEntry.COLUMN_ETAPA + "), " +

                " FOREIGN KEY (" + ChecklistkEntry.FK_TIPMOD + ") REFERENCES " +
                ModeloEntry.TABLE_NAME + " (" + ModeloEntry.COLUMN_TIPMOD + "), " +

                " FOREIGN KEY (" + ChecklistkEntry.FK_SERVIC + ") REFERENCES " +
                ServicoEntry.TABLE_NAME + " (" + ServicoEntry.COLUMN_SERVIC + ") " +
                " );";

        final String SQL_CREATE_CHECKLIST_SENT_CABECALHO_TABLE = "CREATE TABLE " + CabecalhoChecklistEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                CabecalhoChecklistEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                CabecalhoChecklistEntry.COLUMN_Bem + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_Placa + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_Familia + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_Data + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_Hora + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_TpCheck + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_ModCheck + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_SitVeic + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_USER + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_CPFMot + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_AssMot + " TEXT NOT NULL, " +
                CabecalhoChecklistEntry.COLUMN_TIPMOD + " TEXT NOT NULL" +
                " );";

        final String SQL_CREATE_CHECKLIST_SENT_ITEM_TABLE = "CREATE TABLE " + ItemChecklistEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                ItemChecklistEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                ItemChecklistEntry.COLUMN_Etapa + " TEXT, " +
                ItemChecklistEntry.COLUMN_Desc + " TEXT, " +
                ItemChecklistEntry.COLUMN_Critic + " TEXT, " +
                ItemChecklistEntry.COLUMN_Img + " TEXT, " +
                ItemChecklistEntry.COLUMN_TextoComp + " TEXT, " +
                ItemChecklistEntry.COLUMN_data + " TEXT, " +
                ItemChecklistEntry.COLUMN_hora + " TEXT, " +
                ItemChecklistEntry.COLUMN_status + " TEXT" +
                " );";

        final String SQL_CREATE_JSON_SENT_TABLE = "CREATE TABLE " + JsonEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                JsonEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                JsonEntry.COLUMN_JSON + " TEXT, " +
                JsonEntry.COLUMN_DATA + " TEXT, " +
                JsonEntry.COLUMN_HORA + " TEXT " +
                " );";

        final String SQL_CREATE_FUNCIONARIO_TABLE = "CREATE TABLE " + FuncionarioEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                FuncionarioEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                FuncionarioEntry.COLUMN_NOME + " TEXT, " +
                FuncionarioEntry.COLUMN_EMAIL + " TEXT " +
                " );";
        final String SQL_CREATE_LOG_TABLE = "CREATE TABLE " + checklistContract.LogEntry.TABLE_NAME + " (" +
                // Why AutoIncrement here, and not above?
                // Unique keys will be auto-generated in either case.  But for weather
                // forecasting, it's reasonable to assume the user will want information
                // for a certain date and all dates *following*, so the forecast data
                // should be sorted accordingly.
                LogEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +

                // the ID of the location entry associated with this weather data
                LogEntry.COLUMN_JSONLOG + " TEXT " +
                " );";

        sqLiteDatabase.execSQL(SQL_CREATE_USERLIST_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_MOTORISTA_DA4_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_MODELO_TQR_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_ETAPA_TPA_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_SERVICO_ST4_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_FAMILIA_ST6_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_BEM_ST9_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CHECKLIST_TTE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CHECKLIST_SENT_CABECALHO_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_CHECKLIST_SENT_ITEM_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_JSON_SENT_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_FUNCIONARIO_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_MODCHECK_TTD_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_LOG_TABLE);
}

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MotoristaEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + BemEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ModCheckEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ChecklistkEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ModeloEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + EtapaEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ServicoEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + FamiliaEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CabecalhoChecklistEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
