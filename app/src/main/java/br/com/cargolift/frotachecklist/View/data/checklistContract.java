/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.cargolift.frotachecklist.View.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.format.Time;


/**
 * Defines table and column names for the weather database.
 */
public class checklistContract {

    public static final String CONTENT_AUTHORITY = "br.com.cargolift.frotachecklist";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_USERLIST = "userlist";

    public static final String PATH_MOTORISTA_DA4 = "motorista_da4";

    public static final String PATH_BEM_ST9 = "bem_st9";

    public static final String PATH_MODCHECKLIST_TTD = "modchecklist_ttd";

    public static final String PATH_CHECKLIST_TTE = "checklist_tte";

    public static final String PATH_MODELO_TQR = "modelo_tqr";

    public static final String PATH_ETAPA_TPA = "etapa_tpa";

    public static final String PATH_SERVICO_ST4 = "servico_st4";

    public static final String PATH_FAMILIA_ST6 = "familia_st6";

    public static final String PATH_CABECALHO_CHECKLIST = "cabecalhochecklist";

    public static final String PATH_ITEM_CHECKLIST = "itemchecklist";

    public static final String PATH_SENT_JSON = "sentjson";

    public static final String PATH_FUNCIONARIO = "funcionario";

    public static final String PATH_LOG = "log";


    public static final class checklistUserEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USERLIST).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USERLIST;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USERLIST;

        public static final String _ID = "_ID";

        public static final String USERNAME = "username";

        public static final String PASSWORD = "passsword";

        public static final String EMAIL = "email";

        public static final String NOME = "nome";

        public static final String TABLE_NAME = "userlist";




        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildUserUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class MotoristaEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MOTORISTA_DA4).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOTORISTA_DA4;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MOTORISTA_DA4;

        public static final String TABLE_NAME = "motorista_da4";

        public static final String COLUMN_NOME = "nome";

        public static final String COLUMN_CGC = "cgc";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildMotoristaUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class BemEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_BEM_ST9).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BEM_ST9;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_BEM_ST9;

        public static final String TABLE_NAME = "bem_st9";

        public static final String COLUMN_CODBEM = "codbem";

        public static final String COLUMN_NOME = "nome";

        public static final String COLUMN_PLACA = "placa";

        public static final String COLUMN_SITBEM = "sitbem";

        public static final String FK_CODFAM = "codfam";

        public static final String FK_TIPMOD = "tpmod";

        public static final String COLUMN_FABRICA = "fabrica";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildBemUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class ModCheckEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MODCHECKLIST_TTD).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MODCHECKLIST_TTD;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MODCHECKLIST_TTD;

        public static final String TABLE_NAME = "modelochecklist_ttd";

        public static final String FK_CODFAM = "codfam";

        public static final String FK_TIPMOD = "tipmod";

        public static final String COLUMN_SEQFAM = "seqfam";

        public static final String COLUMN_NOMSEQ = "nomseq";

        public static final String COLUMN_NUMSEQ = "numseq";

        public static final String COLUMN_TPCHKL = "tpchkl";

        public static final String COLUMN_MDCHKL = "mdchkl";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildModCheckUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class ChecklistkEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHECKLIST_TTE).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CHECKLIST_TTE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CHECKLIST_TTE;

        public static final String TABLE_NAME = "checklist_tte";

        public static final String FK_CODFAM = "codfam";

        public static final String FK_ETAPA = "etapa";

        public static final String COLUMN_ALTA = "alta";

        public static final String COLUMN_MEDIA = "media";

        public static final String COLUMN_BAIXA = "baixa";

        public static final String FK_SERVIC = "servic";

        public static final String FK_TIPMOD = "tipmod";

        public static final String COLUMN_SEQFAM = "seqfam";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildChecklistUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class CabecalhoChecklistEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CABECALHO_CHECKLIST).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CABECALHO_CHECKLIST;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_CABECALHO_CHECKLIST;

        public static final String TABLE_NAME = "cabecalhochecklist";

        public static final String COLUMN_Bem = "bem";

        public static final String COLUMN_Placa = "placa";

        public static final String COLUMN_Familia = "familia";

        public static final String COLUMN_Data = "data";

        public static final String COLUMN_Hora = "hora";

        public static final String COLUMN_TpCheck = "tpcheck";

        public static final String COLUMN_ModCheck = "modcheck";

        public static final String COLUMN_SitVeic = "sitveic";

        public static final String COLUMN_CPFMot = "cpfmot";

        public static final String COLUMN_AssMot = "assmot";

        public static final String COLUMN_USER = "user";

        public static final String COLUMN_TIPMOD = "tipmod";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildCabecalhoChecklistUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class ItemChecklistEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEM_CHECKLIST).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ITEM_CHECKLIST;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ITEM_CHECKLIST;

        public static final String TABLE_NAME = "itemchecklist";

        public static final String COLUMN_Etapa = "Etapa";

        public static final String COLUMN_Desc = "Desc";

        public static final String COLUMN_Critic = "Critic";

        public static final String COLUMN_Img = "Img";

        public static final String COLUMN_TextoComp = "TextoComp";

        public static final String COLUMN_data = "data";

        public static final String COLUMN_hora = "hora";

        public static final String COLUMN_status = "status";



        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildItemChecklistUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class ModeloEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_MODELO_TQR).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MODELO_TQR;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MODELO_TQR;

        public static final String TABLE_NAME = "modelo_tqr";

        public static final String COLUMN_TIPMOD = "tipmod";

        public static final String COLUMN_DESMOD = "desmod";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildModeloUri() {
            return CONTENT_URI.buildUpon().appendQueryParameter(_ID, "1").build();
        }
    }
    public static final class EtapaEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ETAPA_TPA).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ETAPA_TPA;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ETAPA_TPA;

        public static final String TABLE_NAME = "etapa_tpa";

        public static final String COLUMN_ETAPA = "etapa";

        public static final String COLUMN_DESCRI = "descri";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildEtapaUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class ServicoEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SERVICO_ST4).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SERVICO_ST4;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SERVICO_ST4;

        public static final String TABLE_NAME = "servico_st4";

        public static final String COLUMN_SERVIC = "servico";

        public static final String COLUMN_NOME = "nome";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildServicoUri(long id) {
            return CONTENT_URI.buildUpon().appendQueryParameter(_ID, "1").build();
        }
    }

    public static final class FamiliaEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FAMILIA_ST6).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAMILIA_ST6;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FAMILIA_ST6;

        public static final String TABLE_NAME = "familia_st6";

        public static final String COLUMN_CODFAM = "codfam";

        public static final String COLUMN_NOME = "nome";

        public static final String COLUMN_SHORT_DESC = "short_desc";

        public static Uri buildFamiliaUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }

    public static final class JsonEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_SENT_JSON).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SENT_JSON;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_SENT_JSON;

        public static final String TABLE_NAME = "sentjson";

        public static final String COLUMN_JSON = "json";

        public static final String COLUMN_DATA = "data";

        public static final String COLUMN_HORA = "hora";

        public static Uri buildJsonUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class FuncionarioEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_FUNCIONARIO).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FUNCIONARIO;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_FUNCIONARIO;

        public static final String TABLE_NAME = "funcionario";

        public static final String COLUMN_NOME = "nome";

        public static final String COLUMN_EMAIL = "email";

        public static Uri buildFuncionarioUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }
    public static final class LogEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LOG).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOG;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_LOG;

        public static final String TABLE_NAME = "log";

        public static final String COLUMN_JSONLOG = "jsonLog";

        public static Uri buildLogUri() {
            return CONTENT_URI.buildUpon().build();
        }
    }


}
