package br.com.cargolift.frotachecklist.View;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;
import br.com.cargolift.frotachecklist.View.sync.ChecklistSyncAdapter;

public class Settings extends AppCompatActivity {

    private static final String[] UserList_COLUMNS = {

            checklistContract.checklistUserEntry._ID,
            checklistContract.checklistUserEntry.USERNAME,
            checklistContract.checklistUserEntry.PASSWORD,
            checklistContract.checklistUserEntry.EMAIL,
            checklistContract.checklistUserEntry.NOME
    };

    private static final String[] Log_COLUMNS = {

            checklistContract.LogEntry._ID,
            checklistContract.LogEntry.COLUMN_JSONLOG
    };

    private static final int INDEX_USER_ID = 0;
    private static final int INDEX_USER_USER = 1;
    private static final int INDEX_USER_PASS = 2;
    private static final int INDEX_USER_EMAIL = 3;
    private static final int INDEX_USER_NAME = 4;

    private static final int PROGRESS = 0x1;

    public static ProgressBar mProgress;
    public static TextView usertableversion;
    public static ProgressBar pB1;
    public static Button update;
    public static int mProgressStatus = 0;
    String URLServer;
    String mUser;

    private Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        TextView tvNomeUser = (TextView) findViewById(R.id.LoggedUser);
        Button buttonlogout = (Button) findViewById(R.id.Logout);
        mProgress = (ProgressBar) findViewById(R.id.progress_bar);
        pB1 = (ProgressBar) findViewById(R.id.pB1);
        update = (Button) findViewById(R.id.updateDatabase);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        URLServer = "http://ws.cargolift.com.br:11605/rest/";


        Uri UriValidaLogin = checklistContract.checklistUserEntry.buildUserUri();

        SharedPreferences pref2 = getApplicationContext().getSharedPreferences("LoggedUser", MODE_PRIVATE);
        mUser = pref2.getString("User", null);

        Cursor cur = getApplicationContext().getContentResolver().query(UriValidaLogin, UserList_COLUMNS, checklistContract.checklistUserEntry.USERNAME + " = ?", new String[]{mUser}, "_id ASC");
        int CountEnviosPendentes = cur.getCount();
        cur.moveToFirst();

        if (CountEnviosPendentes > 0){
            tvNomeUser.setText(cur.getString(INDEX_USER_NAME));
        }

        buttonlogout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(Settings.this, MainActivity.class);
                startActivity(intent);
            }

        });

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String TableVersion = pref.getString("key_name5", null);

        if (TableVersion != null){
            usertableversion = (TextView) findViewById(R.id.usertableversion);
            usertableversion.setText(TableVersion);
        }



        update.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                pB1.setVisibility(View.VISIBLE);
                update.setVisibility(View.GONE);
                SharedPreferences pref = getApplicationContext().getSharedPreferences("GETBACK", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();

                editor.putString("VOLTAR", "SIM");

                editor.apply();

                VeriricaServerOn task = new VeriricaServerOn();
                task.execute(URLServer);
            }
        });


        Button startBtn = (Button) findViewById(R.id.LOG);
        startBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (isOnline()){
                    sendEmail task = new sendEmail ();
                    task.execute();
                }
            }
        });
    }
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
    private class sendEmail extends AsyncTask<String, Void, Boolean> {
        MaterialDialog.Builder pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new MaterialDialog.Builder(Settings.this)
                    .title(R.string.SendingEmail)
                    .content(R.string.PleaseWait)
                    .progress(true, 0)
                    .cancelable(false);
            pd.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {





            Uri UriEnviaLog = checklistContract.LogEntry.buildLogUri();
            String msg = "";
            Cursor cur = getApplicationContext().getContentResolver().query(UriEnviaLog, Log_COLUMNS, null, null, "_id ASC");
            int CountLog = cur.getCount();
            cur.moveToFirst();

            if (CountLog > 0){
                for (int k = 0; k < CountLog; k++) {
                    msg = msg + (cur.getString(1));
                    cur.moveToNext();
                }
                cur.close();

                final String username = "ti@cargolift.com.br";
                final String password = "C@rgolift17";

                Properties props = new Properties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", "smtp.office365.com");
                props.put("mail.smtp.port", "587");

                Session session = Session.getInstance(props,
                        new javax.mail.Authenticator() {
                            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                                return new javax.mail.PasswordAuthentication(username, password);
                            }
                        });
                try {
                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress("ti@cargolift.com.br"));
                    message.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse("ti@cargolift.com.br"));
                    message.setSubject("Log de checklists de: " + mUser);
                    message.setText("Teste");

                    MimeBodyPart messageBodyPart = new MimeBodyPart();

                    Multipart multipart = new MimeMultipart();

                    messageBodyPart = new MimeBodyPart();
                    messageBodyPart.setText(msg);
                    multipart.addBodyPart(messageBodyPart);

                    message.setContent(multipart);

                    Transport.send(message);

                    System.out.println("Done");

                    return true;

                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }
        }
            return false;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            boolean bResponse = result;
            if (bResponse)
            {
                Intent intent = new Intent(Settings.this, Settings.class);
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), Checklist.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void SetarProgresso(boolean b, final String formattedDate) {
        if (b){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (usertableversion != null) {
                        usertableversion.setText(formattedDate.substring(1));
                        pB1.setVisibility(View.GONE);
                        update.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }
    @Override
    public void onBackPressed() {

      Intent intent = new Intent(getApplicationContext(), Checklist.class);
      startActivity(intent);
    }

    private class VeriricaServerOn extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Boolean doInBackground(String... params) {

            try {
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection con =  (HttpURLConnection) new URL(params[0]).openConnection();
                con.setRequestMethod("HEAD");
                con.setConnectTimeout(10 * 1000);
                System.out.println(con.getResponseCode());
            }
            catch (java.net.SocketTimeoutException e) {
                return false;
            }catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            boolean bResponse = result;
            if (bResponse)
            {
                new Thread(new Runnable() {

                    public void run() {
                        usertableversion = (TextView) findViewById(R.id.usertableversion);
                        // Update the progress bar
                        mHandler.post(new Runnable() {
                            public void run() {
                                ChecklistSyncAdapter.syncImmediately(getApplicationContext());
                            }
                        });
                    }
                }).start();
            }
            else
            {
                new MaterialDialog.Builder(Settings.this)
                    .title(R.string.server_timeout_title)
                    .content(R.string.server_timeout_desc)
                    .positiveText(R.string.Dialog_OK)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.cancel();
                        }
                    })
                    .show();
            }
        }
    }
}
