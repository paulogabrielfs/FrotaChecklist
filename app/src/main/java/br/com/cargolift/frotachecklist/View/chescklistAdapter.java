package br.com.cargolift.frotachecklist.View;

        import android.content.ContentResolver;
        import android.content.ContentValues;
        import android.content.Context;
        import android.content.SharedPreferences;
        import android.database.Cursor;
        import android.media.Image;
        import android.net.Uri;
        import android.support.v4.widget.CursorAdapter;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.TextView;

        import org.w3c.dom.Text;

        import java.util.Objects;
        import java.util.Vector;

        import br.com.cargolift.frotachecklist.R;
        import br.com.cargolift.frotachecklist.View.data.checklistContract;

        import static android.app.PendingIntent.getActivity;
        import static android.content.Context.MODE_PRIVATE;
        import static java.security.AccessController.getContext;

/**
 * {@link chescklistAdapter} exposes a list of weather forecasts
 * from a {@link android.database.Cursor} to a {@link android.widget.ListView}.
 */
public class chescklistAdapter extends CursorAdapter {
    private final int VIEW_TYPE_TODAY = 0;
    private final int VIEW_TYPE_FUTURE_DAY = 1;
    static final int COL_ITEMCHECKLIST_ID = 0;
    static final int COL_ITEMCHECKLIS_COLUMN_Etapa = 1;
    static final int COL_ITEMCHECKLIS_COLUMN_Desc = 2;
    static final int COL_ITEMCHECKLIS_COLUMN_Critic = 3;
    static final int COL_ITEMCHECKLIS_COLUMN_Img = 4;
    static final int COL_ITEMCHECKLIS_COLUMN_TextoComp = 5;
    static final int COL_ITEMCHECKLIS_COLUMN_data = 6;
    static final int COL_ITEMCHECKLIS_COLUMN_hora = 7;
    static final int COL_ITEMCHECKLIS_COLUMN_status = 8;

    public static class ViewHolder{
        public final TextView EtapaTV;
        public final ImageView ImageTV;
        public final LinearLayout ColorBackground;

        ViewHolder(View view){
            EtapaTV = (TextView) view.findViewById(R.id.TVEtapa);
            ImageTV = (ImageView) view.findViewById(R.id.statuscheck);
            ColorBackground = (LinearLayout) view.findViewById(R.id.ColorBackground);
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        int viewType = getItemViewType(cursor.getPosition());
        int layoutId = -1;

        if (viewType == VIEW_TYPE_TODAY){
            layoutId = R.layout.list_item_check;
        }else if (viewType == VIEW_TYPE_FUTURE_DAY){
            layoutId = R.layout.list_item_check;
        }
        View view =  LayoutInflater.from(context).inflate(layoutId, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    /*
        This is where we fill-in the views with the contents of the cursor.
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        String[] mSELECT_Etapa_ARGUMENTS = {""};
        String SelectEtapaWithCod = null;

        if (cursor == null) {

        } else {
            int teste = cursor.getCount();
            String DescEtapa = cursor.getString(COL_ITEMCHECKLIS_COLUMN_Desc);
            cursor.moveToNext();
            viewHolder.EtapaTV.setText(DescEtapa);
        }
    }

    public chescklistAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public int getItemViewType(int position){
        return (position == 0) ?VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }

    @Override
    public int getViewTypeCount(){
        return 2;
    }
    /**
     * Prepare the weather high/lows for presentation.
     */


}

