package br.com.cargolift.frotachecklist.View;

import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Logger;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;


public class ListaChecklist extends AppCompatActivity {

    private ViewPager pager;
    private MyPagerAdapter pagerAdapter;
    MaterialTabHost tabHost;
    private Resources res;
    int positionfinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_checklist);
        res = this.getResources();

        Toolbar toolbar = (Toolbar) this.findViewById(R.id.my_toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        this.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pager = (ViewPager) findViewById(R.id.pager);
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        ViewPager.OnPageChangeListener pagechangelistener =new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {

                pagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {


            }

            @Override
            public void onPageScrollStateChanged(int arg0) {


            }
        };
        pager.setOnPageChangeListener(pagechangelistener);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(pager);

        viewPagerTab.setOnPageChangeListener(pagechangelistener);



//        tabHost = (MaterialTabHost) this.findViewById(R.id.tabHost);
//        pager = (ViewPager) this.findViewById(R.id.pager);
//        // init view pager
//        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        pager.setAdapter(pagerAdapter);
//        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
//            @Override
//            public void onPageSelected(int position) {
//                // when user do a swipe the selected tab change
//                tabHost.setSelectedNavigationItem(position);
//                pagerAdapter.notifyDataSetChanged();
//            }
//        });
//        // insert all tabs from pagerAdapter data
//        for (int i = 0; i < pagerAdapter.getCount(); i++) {
//            tabHost.addTab(
//                    tabHost.newTab()
//                            .setIcon(getIcon(i))
//                            .setTabListener(this)
//            );
//        }


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final String SelectEtapasWithHoraAndData = checklistContract.CabecalhoChecklistEntry.COLUMN_Data + " = ?" + " AND " + checklistContract.CabecalhoChecklistEntry.COLUMN_Hora + " = ?";

        final String SelectItensasWithHoraAndData = checklistContract.ItemChecklistEntry.COLUMN_data + " = ?" + " AND " + checklistContract.ItemChecklistEntry.COLUMN_hora + " = ?";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat hora = new SimpleDateFormat("kk:mm");
        final String dataformatada = data.format(c.getTime());
        final String horafortmatada = hora.format(c.getTime());
        switch (item.getItemId()) {
            case android.R.id.home:
                new MaterialDialog.Builder(ListaChecklist.this)
                        .title(R.string.attention)
                        .content(R.string.lostallproccess)
                        .positiveText(R.string.Dialog_OK)
                        .negativeText(R.string.cancelbutton)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                getApplicationContext().getContentResolver().delete(checklistContract.ItemChecklistEntry.CONTENT_URI,
                                        SelectItensasWithHoraAndData, new String[]{dataformatada,horafortmatada});

                                getApplicationContext().getContentResolver().delete(checklistContract.CabecalhoChecklistEntry.CONTENT_URI,
                                        SelectEtapasWithHoraAndData, new String[]{dataformatada,horafortmatada});

                                Intent intent = new Intent(getApplicationContext(), Checklist.class);
                                startActivity(intent);

                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.cancel();
                            }
                        })
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onBackPressed() {
        final String SelectEtapasWithHoraAndData = checklistContract.CabecalhoChecklistEntry.COLUMN_Data + " = ?" + " AND " + checklistContract.CabecalhoChecklistEntry.COLUMN_Hora + " = ?";

        final String SelectItensasWithHoraAndData = checklistContract.ItemChecklistEntry.COLUMN_data + " = ?" + " AND " + checklistContract.ItemChecklistEntry.COLUMN_hora + " = ?";
        Calendar c = Calendar.getInstance();
        SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat hora = new SimpleDateFormat("kk:mm");
        final String dataformatada = data.format(c.getTime());
        final String horafortmatada = hora.format(c.getTime());
        new MaterialDialog.Builder(ListaChecklist.this)
                .title(R.string.attention)
                .content(R.string.lostallproccess)
                .positiveText(R.string.Dialog_OK)
                .negativeText(R.string.cancelbutton)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        getApplicationContext().getContentResolver().delete(checklistContract.ItemChecklistEntry.CONTENT_URI,
                                SelectItensasWithHoraAndData, new String[]{dataformatada,horafortmatada});

                        getApplicationContext().getContentResolver().delete(checklistContract.CabecalhoChecklistEntry.CONTENT_URI,
                                SelectEtapasWithHoraAndData, new String[]{dataformatada,horafortmatada});

                        Intent intent = new Intent(getApplicationContext(), Checklist.class);
                        startActivity(intent);

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.cancel();
                    }
                })
                .show();
    }
//    @Override
//    public void onTabSelected(MaterialTab tab) {
//        pager.setCurrentItem(tab.getPosition());
//    }
//
//    @Override
//    public void onTabReselected(MaterialTab tab) {
//    }
//
//    @Override
//    public void onTabUnselected(MaterialTab tab) {
//    }
//
//    private class ViewPagerAdapter extends FragmentStatePagerAdapter {
//        public ViewPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//        public Fragment getItem(int position) {
//            position = pager.getCurrentItem();
//            return new ListaChecklistFragmentPendente();
//        }
//        @Override
//        public int getCount() {
//            return 3;
//        }
//        @Override
//        public CharSequence getPageTitle(int position) {
//            switch(position) {
//                case 0: return "tab 1";
//                case 1: return "tab 2";
//                case 2: return "tab 3";
//                default: return null;
//            }
//        }
//    }
//    /*
//    * It doesn't matter the color of the icons, but they must have solid colors
//    */
//    private Drawable getIcon(int position) {
//        switch(position) {
//            case 0:
//                return res.getDrawable(R.drawable.ic_person_black_24dp);
//            case 1:
//                return res.getDrawable(R.drawable.ic_group_black_24dp);
//            case 2:
//                return res.getDrawable(R.drawable.ic_notifications_off_white_24dp);
//        }
//        return null;
//    }
private class MyPagerAdapter extends FragmentPagerAdapter {

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch(pos) {

            case 0: return new ListaChecklistFragmentPendente();
            case 1: return new ListaChecklistFragmentAprovado();
            case 2: return new ListaChecklistFragmentReprovado();
            default: return new ListaChecklistFragmentPendente();
        }
    }
    @Override
    public int getItemPosition(Object object) {

        return POSITION_NONE;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch(position) {
            case 0: return "Pendentes";
            case 1: return "Aprovados";
            case 2: return "Reprovados";
            default: return "Pendentes";
        }
    }
    @Override
    public int getCount() {
        return 3;
    }
}

}
