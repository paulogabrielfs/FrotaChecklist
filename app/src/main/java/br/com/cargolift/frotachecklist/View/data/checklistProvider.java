/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.cargolift.frotachecklist.View.data;

import android.annotation.TargetApi;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class checklistProvider extends ContentProvider {

    // The URI Matcher used by this content provider.
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private checklistDbHelper mOpenHelper;

//    static final int WEATHER = 100;
//    static final int WEATHER_WITH_LOCATION = 101;
//    static final int WEATHER_WITH_LOCATION_AND_DATE = 102;
//    static final int LOCATION = 300;

    static final int MOTORISTA_DA4 = 100;
    static final int BEM_ST9 = 200;
    static final int MODCHECKLIST_TTD = 300;
    static final int USERLIST = 400;
    static final int CHECKLIST_TTE = 500;
    static final int MODELO_TQR = 600;
    static final int ETAPA_TPA = 700;
    static final int SERVICO_ST4 = 800;
    static final int FAMILIA_ST6 = 900;
    static final int CABECALHO_CHECKLIST = 1000;
    static final int ITEM_CHECKLIST = 1100;
    static final int SENT_JSON = 1200;
    static final int FUNCIONARIO = 1300;
    static final int LOG = 1400;



//    private static final SQLiteQueryBuilder sWeatherByLocationSettingQueryBuilder;
//
//    static{
//        sWeatherByLocationSettingQueryBuilder = new SQLiteQueryBuilder();
//
//        //This is an inner join which looks like
//        //weather INNER JOIN location ON weather.location_id = location._id
//        sWeatherByLocationSettingQueryBuilder.setTables(
//                checklistContract.checklistEntry.TABLE_NAME + " INNER JOIN " +
//                        checklistContract.LocationEntry.TABLE_NAME +
//                        " ON " + checklistContract.checklistEntry.TABLE_NAME +
//                        "." + checklistContract.checklistEntry.COLUMN_LOC_KEY +
//                        " = " + checklistContract.LocationEntry.TABLE_NAME +
//                        "." + checklistContract.LocationEntry._ID);
//    }
//
//    //location.location_setting = ?
//    private static final String sLocationSettingSelection =
//            checklistContract.LocationEntry.TABLE_NAME+
//                    "." + checklistContract.LocationEntry.COLUMN_LOCATION_SETTING + " = ? ";
//
//    //location.location_setting = ? AND date >= ?
//    private static final String sLocationSettingWithStartDateSelection =
//            checklistContract.LocationEntry.TABLE_NAME+
//                    "." + checklistContract.LocationEntry.COLUMN_LOCATION_SETTING + " = ? AND " +
//                    checklistContract.checklistEntry.COLUMN_DATE + " >= ? ";
//
//    //location.location_setting = ? AND date = ?
//    private static final String sLocationSettingAndDaySelection =
//            checklistContract.LocationEntry.TABLE_NAME +
//                    "." + checklistContract.LocationEntry.COLUMN_LOCATION_SETTING + " = ? AND " +
//                    checklistContract.checklistEntry.COLUMN_DATE + " = ? ";
//
//    private Cursor getWeatherByLocationSetting(Uri uri, String[] projection, String sortOrder) {
//        String locationSetting = checklistContract.checklistEntry.getLocationSettingFromUri(uri);
//        long startDate = checklistContract.checklistEntry.getStartDateFromUri(uri);
//
//        String[] selectionArgs;
//        String selection;
//
//        if (startDate == 0) {
//            selection = sLocationSettingSelection;
//            selectionArgs = new String[]{locationSetting};
//        } else {
//            selectionArgs = new String[]{locationSetting, Long.toString(startDate)};
//            selection = sLocationSettingWithStartDateSelection;
//        }
//
//        return sWeatherByLocationSettingQueryBuilder.query(mOpenHelper.getReadableDatabase(),
//                projection,
//                selection,
//                selectionArgs,
//                null,
//                null,
//                sortOrder
//        );
//    }
//
//    private Cursor getWeatherByLocationSettingAndDate(
//            Uri uri, String[] projection, String sortOrder) {
//        String locationSetting = checklistContract.checklistEntry.getLocationSettingFromUri(uri);
//        long date = checklistContract.checklistEntry.getDateFromUri(uri);
//
//        return sWeatherByLocationSettingQueryBuilder.query(mOpenHelper.getReadableDatabase(),
//                projection,
//                sLocationSettingAndDaySelection,
//                new String[]{locationSetting, Long.toString(date)},
//                null,
//                null,
//                sortOrder
//        );
//    }

    /*
        Students: Here is where you need to create the UriMatcher. This UriMatcher will
        match each URI to the WEATHER, WEATHER_WITH_LOCATION, WEATHER_WITH_LOCATION_AND_DATE,
        and LOCATION integer constants defined above.  You can test this by uncommenting the
        testUriMatcher test within TestUriMatcher.
     */
    static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = checklistContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, checklistContract.PATH_USERLIST, USERLIST);
        matcher.addURI(authority, checklistContract.PATH_MOTORISTA_DA4, MOTORISTA_DA4);
        matcher.addURI(authority, checklistContract.PATH_BEM_ST9, BEM_ST9);
        matcher.addURI(authority, checklistContract.PATH_MODCHECKLIST_TTD, MODCHECKLIST_TTD);
        matcher.addURI(authority, checklistContract.PATH_CHECKLIST_TTE, CHECKLIST_TTE);
        matcher.addURI(authority, checklistContract.PATH_MODELO_TQR, MODELO_TQR);
        matcher.addURI(authority, checklistContract.PATH_ETAPA_TPA, ETAPA_TPA);
        matcher.addURI(authority, checklistContract.PATH_SERVICO_ST4, SERVICO_ST4);
        matcher.addURI(authority, checklistContract.PATH_FAMILIA_ST6, FAMILIA_ST6);
        matcher.addURI(authority, checklistContract.PATH_CABECALHO_CHECKLIST, CABECALHO_CHECKLIST);
        matcher.addURI(authority, checklistContract.PATH_ITEM_CHECKLIST, ITEM_CHECKLIST);
        matcher.addURI(authority, checklistContract.PATH_SENT_JSON, SENT_JSON);
        matcher.addURI(authority, checklistContract.PATH_FUNCIONARIO, FUNCIONARIO);
        matcher.addURI(authority, checklistContract.PATH_LOG, LOG);
//        matcher.addURI(authority, checklistContract.PATH_WEATHER + "/*", WEATHER_WITH_LOCATION);
//        matcher.addURI(authority, checklistContract.PATH_WEATHER, WEATHER);
//        matcher.addURI(authority, checklistContract.PATH_WEATHER +"/*/#", WEATHER_WITH_LOCATION_AND_DATE);
//
//        matcher.addURI(authority, checklistContract.PATH_LOCATION, LOCATION);
        // 1) The code passed into the constructor represents the code to return for the root
        // URI.  It's common to use NO_MATCH as the code for this case. Add the constructor below.


        // 2) Use the addURI function to match each of the types.  Use the constants from
        // checklistContract to help define the types to the UriMatcher.


        // 3) Return the new matcher!
        return matcher;
    }

    /*
        Students: We've coded this for you.  We just create a new WeatherDbHelper for later use
        here.
     */
    @Override
    public boolean onCreate() {
        mOpenHelper = new checklistDbHelper(getContext());
        return true;
    }

    /*
        Students: Here's where you'll code the getType function that uses the UriMatcher.  You can
        test this by uncommenting testGetType in TestProvider.
     */
    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            // Student: Uncomment and fill out these two cases
//            case WEATHER_WITH_LOCATION_AND_DATE:
//            case WEATHER_WITH_LOCATION:
//            case WEATHER:
//                return checklistContract.checklistEntry.CONTENT_TYPE;
            case MOTORISTA_DA4:
                return checklistContract.MotoristaEntry.CONTENT_TYPE;
            case BEM_ST9:
                return checklistContract.BemEntry.CONTENT_TYPE;
            case MODCHECKLIST_TTD:
                return checklistContract.ModCheckEntry.CONTENT_TYPE;
            case USERLIST:
                return checklistContract.checklistUserEntry.CONTENT_TYPE;
            case CHECKLIST_TTE:
                return checklistContract.ChecklistkEntry.CONTENT_TYPE;
            case MODELO_TQR:
                return checklistContract.ModeloEntry.CONTENT_TYPE;
            case ETAPA_TPA:
                return checklistContract.EtapaEntry.CONTENT_TYPE;
            case SERVICO_ST4:
                return checklistContract.ServicoEntry.CONTENT_TYPE;
            case FAMILIA_ST6:
                return checklistContract.FamiliaEntry.CONTENT_TYPE;
            case CABECALHO_CHECKLIST:
            return checklistContract.CabecalhoChecklistEntry.CONTENT_TYPE;
            case ITEM_CHECKLIST:
                return checklistContract.ItemChecklistEntry.CONTENT_TYPE;
            case SENT_JSON:
                return checklistContract.JsonEntry.CONTENT_TYPE;
            case FUNCIONARIO:
                return checklistContract.FuncionarioEntry.CONTENT_TYPE;
            case LOG:
                return checklistContract.LogEntry.CONTENT_TYPE;
//            case LOCATION:
//                return checklistContract.LocationEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        // Here's the switch statement that, given a URI, will determine what kind of request it is,
        // and query the database accordingly.
        Cursor retCursor;
        switch (sUriMatcher.match(uri)) {
            // "weather/*/*"
//            case WEATHER_WITH_LOCATION_AND_DATE:
//            {
//                retCursor = getWeatherByLocationSettingAndDate(uri, projection, sortOrder);
//                break;
//            }
//            // "weather/*"
//            case WEATHER_WITH_LOCATION: {
//                retCursor = getWeatherByLocationSetting(uri, projection, sortOrder);
//                break;
//            }
//            // "weather"
//            case WEATHER: {
//                retCursor = null;
//                break;
//            }
            case USERLIST:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.checklistUserEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CHECKLIST_TTE:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.ChecklistkEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ETAPA_TPA:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.EtapaEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case BEM_ST9:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.BemEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case FAMILIA_ST6:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.FamiliaEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case CABECALHO_CHECKLIST:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.CabecalhoChecklistEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case ITEM_CHECKLIST:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.ItemChecklistEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case SENT_JSON:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.JsonEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case LOG:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.LogEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case MOTORISTA_DA4: {
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.MotoristaEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
            }
                break;
            case FUNCIONARIO:{
                    retCursor = mOpenHelper.getReadableDatabase().query(
                            checklistContract.FuncionarioEntry.TABLE_NAME,
                            projection,
                            selection,
                            selectionArgs,
                            null,
                            null,
                            sortOrder
                    );
                    break;
            }
            case MODCHECKLIST_TTD:{
                retCursor = mOpenHelper.getReadableDatabase().query(
                        checklistContract.ModCheckEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            // "location"
//            case LOCATION: {
//                retCursor = null;
//                break;
//            }

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    /*
        Student: Add the ability to insert Locations to the implementation of this function.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
//            case WEATHER: {
//                normalizeDate(values);
//                long _id = db.insert(checklistContract.checklistEntry.TABLE_NAME, null, values);
//                if ( _id > 0 )
//                    returnUri = checklistContract.checklistEntry.buildWeatherUri(_id);
//                else
//                    throw new android.database.SQLException("Failed to insert row into " + uri);
//                break;
//            }
            case MOTORISTA_DA4: {
                long _id = db.insert(checklistContract.MotoristaEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.MotoristaEntry.buildMotoristaUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case BEM_ST9: {
                long _id = db.insert(checklistContract.BemEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.BemEntry.buildBemUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case MODCHECKLIST_TTD: {
                long _id = db.insert(checklistContract.ModCheckEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.ModCheckEntry.buildModCheckUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case USERLIST: {
                long _id = db.insert(checklistContract.checklistUserEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.checklistUserEntry.buildUserUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case CHECKLIST_TTE: {
                long _id = db.insert(checklistContract.ChecklistkEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.ChecklistkEntry.buildChecklistUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case MODELO_TQR: {
                long _id = db.insert(checklistContract.ModeloEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.ModeloEntry.buildModeloUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ETAPA_TPA: {
                long _id = db.insert(checklistContract.EtapaEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.EtapaEntry.buildEtapaUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SERVICO_ST4: {
                long _id = db.insert(checklistContract.ServicoEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.ServicoEntry.buildServicoUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case FAMILIA_ST6: {
                long _id = db.insert(checklistContract.FamiliaEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.FamiliaEntry.buildFamiliaUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case CABECALHO_CHECKLIST: {
                long _id = db.insert(checklistContract.CabecalhoChecklistEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.CabecalhoChecklistEntry.buildCabecalhoChecklistUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case ITEM_CHECKLIST: {
                long _id = db.insert(checklistContract.ItemChecklistEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.ItemChecklistEntry.buildItemChecklistUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SENT_JSON: {
                long _id = db.insert(checklistContract.JsonEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.JsonEntry.buildJsonUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case LOG: {
                long _id = db.insert(checklistContract.LogEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.LogEntry.buildLogUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case FUNCIONARIO: {
                long _id = db.insert(checklistContract.FuncionarioEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = checklistContract.FuncionarioEntry.buildFuncionarioUri();
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) throws UnsupportedOperationException {

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        if (null == selection) selection = "1";

        switch (match){
            case MOTORISTA_DA4:
                rowsDeleted = db.delete(checklistContract.MotoristaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case BEM_ST9:
                rowsDeleted = db.delete(checklistContract.BemEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case MODCHECKLIST_TTD:
                rowsDeleted = db.delete(checklistContract.ModCheckEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case USERLIST:
                rowsDeleted = db.delete(checklistContract.checklistUserEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CHECKLIST_TTE:
                rowsDeleted = db.delete(checklistContract.ChecklistkEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case MODELO_TQR:
                rowsDeleted = db.delete(checklistContract.ModeloEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case ETAPA_TPA:
                rowsDeleted = db.delete(checklistContract.EtapaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SERVICO_ST4:
                rowsDeleted = db.delete(checklistContract.ServicoEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case FAMILIA_ST6:
                rowsDeleted = db.delete(checklistContract.FamiliaEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CABECALHO_CHECKLIST:
                rowsDeleted = db.delete(checklistContract.CabecalhoChecklistEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case ITEM_CHECKLIST:
                rowsDeleted = db.delete(checklistContract.ItemChecklistEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case SENT_JSON:
                rowsDeleted = db.delete(checklistContract.JsonEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case LOG:
                rowsDeleted = db.delete(checklistContract.LogEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case FUNCIONARIO:
                rowsDeleted = db.delete(checklistContract.FuncionarioEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (rowsDeleted != 0 ){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

//    private void normalizeDate(ContentValues values) {
//        // normalize the date value
//        if (values.containsKey(checklistContract.checklistEntry.COLUMN_DATE)) {
//            long dateValue = values.getAsLong(checklistContract.checklistEntry.COLUMN_DATE);
//            values.put(checklistContract.checklistEntry.COLUMN_DATE, checklistContract.normalizeDate(dateValue));
//        }
//    }

    @Override
    public int update(
            Uri uri, ContentValues values, String selection, String[] selectionArgs) throws UnsupportedOperationException {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match){
            case MOTORISTA_DA4:
                rowsUpdated = db.update(checklistContract.MotoristaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case BEM_ST9:
                rowsUpdated = db.update(checklistContract.BemEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case MODCHECKLIST_TTD:
                rowsUpdated = db.update(checklistContract.ModCheckEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case USERLIST:
                rowsUpdated = db.update(checklistContract.checklistUserEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CHECKLIST_TTE:
                rowsUpdated = db.update(checklistContract.ChecklistkEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case MODELO_TQR:
                rowsUpdated = db.update(checklistContract.ModeloEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ETAPA_TPA:
                rowsUpdated = db.update(checklistContract.EtapaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case SERVICO_ST4:
                rowsUpdated = db.update(checklistContract.ServicoEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case FAMILIA_ST6:
                rowsUpdated = db.update(checklistContract.FamiliaEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CABECALHO_CHECKLIST:
                rowsUpdated = db.update(checklistContract.CabecalhoChecklistEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case ITEM_CHECKLIST:
                rowsUpdated = db.update(checklistContract.ItemChecklistEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case SENT_JSON:
                rowsUpdated = db.update(checklistContract.JsonEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case LOG:
                rowsUpdated = db.update(checklistContract.LogEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case FUNCIONARIO:
                rowsUpdated = db.update(checklistContract.FuncionarioEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int returnCount = 0;
        switch (match) {
            case MOTORISTA_DA4:
                db.beginTransaction();
                 returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.MotoristaEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case BEM_ST9:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.BemEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case MODCHECKLIST_TTD:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.ModCheckEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case USERLIST:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.checklistUserEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case CHECKLIST_TTE:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.ChecklistkEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {

                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case MODELO_TQR:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.ModeloEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case ETAPA_TPA:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.EtapaEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case SERVICO_ST4:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.ServicoEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case FAMILIA_ST6:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.FamiliaEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
            break;
            case CABECALHO_CHECKLIST:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.CabecalhoChecklistEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case ITEM_CHECKLIST:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.ItemChecklistEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case SENT_JSON:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.JsonEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case LOG:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.LogEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            case FUNCIONARIO:
                db.beginTransaction();
                returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insert(checklistContract.FuncionarioEntry.TABLE_NAME, null, value);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                }
                finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                break;
            default:
                return super.bulkInsert(uri, values);
        }
        return returnCount;
    }

    // You do not need to call this method. This is a method specifically to assist the testing
    // framework in running smoothly. You can read more at:
    // http://developer.android.com/reference/android/content/ContentProvider.html#shutdown()
    @Override
    @TargetApi(11)
    public void shutdown() {
        mOpenHelper.close();
        super.shutdown();
    }
}