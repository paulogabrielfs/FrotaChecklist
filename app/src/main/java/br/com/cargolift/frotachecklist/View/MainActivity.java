package br.com.cargolift.frotachecklist.View;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.TimeoutException;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;
import br.com.cargolift.frotachecklist.View.sync.ChecklistSyncAdapter;

import static android.util.Base64.encodeToString;
import static java.lang.Boolean.TRUE;
import static java.security.AccessController.getContext;


/**
 * Android login screen Activity
 */
public class MainActivity extends Activity implements LoaderCallbacks<Cursor> {



    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    private static final String DUMMY_CREDENTIALS = ":";

    private UserLoginTask userLoginTask = null;
    private View loginFormView;
    private View progressView;
    private AutoCompleteTextView emailTextView;
    private EditText passwordTextView;
    int IncorrectPass = 0;
    boolean ErrorURL = false;

    private static final String[] UserList_COLUMNS = {

            checklistContract.checklistUserEntry._ID,
            checklistContract.checklistUserEntry.USERNAME,
            checklistContract.checklistUserEntry.PASSWORD,
            checklistContract.checklistUserEntry.EMAIL,
            checklistContract.checklistUserEntry.NOME
    };

    private static final int INDEX_USER_ID = 0;
    private static final int INDEX_USER_USER = 1;
    private static final int INDEX_USER_PASS = 2;
    private static final int INDEX_USER_EMAIL = 3;
    private static final int INDEX_USER_NAME = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        emailTextView = (AutoCompleteTextView) findViewById(R.id.email);
        loadAutoComplete();

        passwordTextView = (EditText) findViewById(R.id.password);
        passwordTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    return true;
                }
                return false;
            }
        });

        Button loginButton = (Button) findViewById(R.id.email_sign_in_button);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                SharedPreferences pref = getApplicationContext().getSharedPreferences("Jalogou", MODE_PRIVATE);
                String TableVersion = pref.getString("Logou", null);

                if (Objects.equals(TableVersion, "SIM")){
                    initLogin();
                }else {
                    if (verificaConexao()) {
                        initLogin();
                    } else {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title(R.string.NoConnection)
                                .content(R.string.FirstLogin)
                                .positiveText(R.string.Dialog_OK)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        dialog.cancel();

                                    }
                                })
                                .show();
                    }
                }
            }
        });

        loginFormView = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);

        ChecklistSyncAdapter.initializeSyncAdapter(this);

    }

    private void loadAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Validate Login form and authenticate.
     */
    public void initLogin() {
        showProgress(true);
        if (userLoginTask != null) {
            return;
        }

        emailTextView.setError(null);
        passwordTextView.setError(null);

        String email = emailTextView.getText().toString();
        String password = passwordTextView.getText().toString();

        boolean cancelLogin = false;
        View focusView = null;

        /*if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            passwordTextView.setError(getString(R.string.invalid_password));
            focusView = passwordTextView;
            cancelLogin = true;
        }

        if (TextUtils.isEmpty(email)) {
            emailTextView.setError(getString(R.string.field_required));
            focusView = emailTextView;
            cancelLogin = true;
        } else if (!isEmailValid(email)) {
            emailTextView.setError(getString(R.string.invalid_email));
            focusView = emailTextView;
            cancelLogin = true;
        }

        if (cancelLogin) {
            // error in login
            focusView.requestFocus();
        } else {
            // show progress spinner, and start background task to login
            userLoginTask = new UserLoginTask(email, password);
            userLoginTask.execute((Void) null);*/
        new UserLoginTask(email, password).execute();
        //}
    }

    private boolean isEmailValid(String email) {
        //add your own logic
        return true;
    }

    private boolean isPasswordValid(String password) {
        //add your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);

            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            loginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


//    @Override
//    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
//        return new CursorLoader(this,
//                // Retrieve data rows for the device user's 'profile' contact.
//                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
//                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,
//
//                // Select only email addresses.
//                ContactsContract.Contacts.Data.MIMETYPE +
//                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
//                .CONTENT_ITEM_TYPE},
//
//                // Show primary email addresses first. Note that there won't be
//                // a primary email address if the user hasn't specified one.
//                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
//    }
//
//    @Override
//    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
//        List<String> emails = new ArrayList<String>();
//        cursor.moveToFirst();
//        while (!cursor.isAfterLast()) {
//            emails.add(cursor.getString(ProfileQuery.ADDRESS));
//            cursor.moveToNext();
//        }
//
//        addEmailsToAutoComplete(emails);
//    }
//
//    @Override
//    public void onLoaderReset(Loader<Cursor> cursorLoader) {
//
//    }
//
//    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
//        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
//        ArrayAdapter<String> adapter =
//                new ArrayAdapter<String>(MainActivity.this,
//                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);
//
//        emailTextView.setAdapter(adapter);
//    }
//
//
//    private interface ProfileQuery {
//        String[] PROJECTION = {
//                ContactsContract.CommonDataKinds.Email.ADDRESS,
//                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
//        };
//
//        int ADDRESS = 0;
//        int IS_PRIMARY = 1;
//    }

    /**
     * Async Login Task to authenticate
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String emailStr;
        private final String passwordStr;

        UserLoginTask(String email, String password) {
            emailStr = email;
            passwordStr = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Uri UriValidaLogin = checklistContract.checklistUserEntry.buildUserUri();

            Cursor cur = getApplicationContext().getContentResolver().query(UriValidaLogin, UserList_COLUMNS, checklistContract.checklistUserEntry.USERNAME + " = ?", new String[]{emailStr}, "_id ASC");
            int CountEnviosPendentes = cur.getCount();

            if (CountEnviosPendentes > 0) {
                cur.moveToFirst();
                String SenhaBanco = cur.getString(INDEX_USER_PASS);


                if(!Objects.equals(passwordStr, SenhaBanco)){
                    IncorrectPass = IncorrectPass + 1;

                    if (!(IncorrectPass >= 3)){
                        return false;
                    }else {
                        JsonObject Login = new JsonObject();

                        Login.addProperty("user", emailStr);
                        Login.addProperty("pass", passwordStr);

                        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
                        System.out.println(gson.toJson(Login));

                        try {
                            URL URL_PADRAO = new URL("http://ws.cargolift.com.br:11605/rest/REST003/ValidaLogin");
                            String name = "webservice";
                            String password = "*3uXagUwEWAb";
                            String authString = name + ":" + password;
                            String authStringEnc = encodeToString(authString.getBytes(), Base64.DEFAULT);

                            HttpURLConnection urlConnectionValidaLogin = (HttpURLConnection) URL_PADRAO.openConnection();
                            urlConnectionValidaLogin.setDoInput(true);
                            urlConnectionValidaLogin.setDoOutput(true);
                            urlConnectionValidaLogin.setUseCaches(false);
                            urlConnectionValidaLogin.setConnectTimeout(8 * 1000);
                            urlConnectionValidaLogin.setRequestMethod("POST");
                            urlConnectionValidaLogin.setRequestProperty("Content-Type", "application/json");
                            urlConnectionValidaLogin.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
                            urlConnectionValidaLogin.setRequestProperty("connection", "close");

                            DataOutputStream wr = new DataOutputStream(urlConnectionValidaLogin.getOutputStream());
                            wr.writeBytes(gson.toJson(Login));

                            wr.flush();
                            wr.close();


                            int code = urlConnectionValidaLogin.getResponseCode();

                            System.out.print(code);

                            if (code == 201) {

                                InputStream is = urlConnectionValidaLogin.getInputStream();
                                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                                String line;
                                StringBuffer response = new StringBuffer();
                                while ((line = rd.readLine()) != null) {
                                    response.append(line);
                                    response.append('\r');
                                }

                                rd.close();

                                String JsonSTR = response.toString();

                                final String OWM_cId = "Id";
                                final String OWM_cUsuario = "Usuario";
                                final String OWM_cEmail = "E-mail";
                                final String OWM_cDepartamento = "Departamento";
                                final String OWM_Cargo = "Cargo";
                                final String OWM_Status = "Status";

                                JSONObject checklistJson = null;
                                try {
                                    checklistJson = new JSONObject(JsonSTR);
                                    String id;
                                    String usuario;
                                    String senha;
                                    String email;
                                    String nome;


                                    id = checklistJson.getString(OWM_cId);
                                    usuario = emailStr;
                                    senha = passwordStr;
                                    email = checklistJson.getString(OWM_cEmail);
                                    nome = checklistJson.getString(OWM_cUsuario);


                                    Vector<ContentValues> cVVectorModCheck = new Vector<ContentValues>();
                                    ContentValues ModCheckValues = new ContentValues();

                                    ModCheckValues.put(checklistContract.checklistUserEntry._ID, id);
                                    ModCheckValues.put(checklistContract.checklistUserEntry.USERNAME, usuario);
                                    ModCheckValues.put(checklistContract.checklistUserEntry.PASSWORD, senha);
                                    ModCheckValues.put(checklistContract.checklistUserEntry.EMAIL, email);
                                    ModCheckValues.put(checklistContract.checklistUserEntry.NOME, nome);

                                    cVVectorModCheck.add(ModCheckValues);

                                    getApplicationContext().getContentResolver().update(checklistContract.checklistUserEntry.CONTENT_URI, ModCheckValues,
                                            checklistContract.checklistUserEntry._ID + " = ?", new String[]{id});

                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("LoggedUser", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = pref.edit();

                                    editor.putString("User", emailStr);
                                    editor.putString("Mail", email);

                                    editor.apply();
                                    return true;
                                } catch (JSONException e) {
                                    return false;
                                }


                            } else if (code == 403) {
                                return false;
                            } else if (code == 401) {
                                return false;
                            }
                            return true;


                        }
                        catch (java.net.SocketTimeoutException e) {
                            ErrorURL = TRUE;
                            return false;
                        }catch (IOException e) {
                            e.printStackTrace();
                            return false;
                        }
                    }
                }else{
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("LoggedUser", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();


                    editor.putString("User", emailStr);
                    editor.putString("Mail", cur.getString(INDEX_USER_EMAIL));

                    editor.apply();
                    return true;
                }
            } else {

                JsonObject Login = new JsonObject();

                Login.addProperty("user", emailStr);
                Login.addProperty("pass", passwordStr);

                Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
                System.out.println(gson.toJson(Login));

                try {
                    URL URL_PADRAO = new URL("http://ws.cargolift.com.br:11605/rest/REST003/ValidaLogin");
                    String name = "webservice";
                    String password = "*3uXagUwEWAb";
                    String authString = name + ":" + password;
                    String authStringEnc = encodeToString(authString.getBytes(), Base64.DEFAULT);

                    HttpURLConnection urlConnectionValidaLogin = (HttpURLConnection) URL_PADRAO.openConnection();
                    urlConnectionValidaLogin.setDoInput(true);
                    urlConnectionValidaLogin.setDoOutput(true);
                    urlConnectionValidaLogin.setUseCaches(false);
                    urlConnectionValidaLogin.setConnectTimeout(8 * 1000);
                    urlConnectionValidaLogin.setRequestMethod("POST");
                    urlConnectionValidaLogin.setRequestProperty("Content-Type", "application/json");
                    urlConnectionValidaLogin.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
                    urlConnectionValidaLogin.setRequestProperty("connection", "close");

                    DataOutputStream wr = new DataOutputStream(urlConnectionValidaLogin.getOutputStream());
                    wr.writeBytes(gson.toJson(Login));

                    wr.flush();
                    wr.close();

                    int code = urlConnectionValidaLogin.getResponseCode();

                    if (code == 201) {

                        InputStream is = urlConnectionValidaLogin.getInputStream();
                        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                        String line;
                        StringBuffer response = new StringBuffer();
                        while ((line = rd.readLine()) != null) {
                            response.append(line);
                            response.append('\r');
                        }

                        rd.close();

                        String JsonSTR = response.toString();

                        final String OWM_cId = "Id";
                        final String OWM_cUsuario = "Usuario";
                        final String OWM_cEmail = "E-mail";
                        final String OWM_cDepartamento = "Departamento";
                        final String OWM_Cargo = "Cargo";
                        final String OWM_Status = "Status";

                        JSONObject checklistJson = null;
                        try {
                            checklistJson = new JSONObject(JsonSTR);
                        String id;
                                String usuario;
                                String senha;
                                String email;
                                String nome;


                        id = checklistJson.getString(OWM_cId);
                        usuario = emailStr;
                        senha = passwordStr;
                        email = checklistJson.getString(OWM_cEmail);
                        nome = checklistJson.getString(OWM_cUsuario);
                            


                        Vector<ContentValues> cVVectorModCheck = new Vector<ContentValues>();
                        ContentValues ModCheckValues = new ContentValues();

                        ModCheckValues.put(checklistContract.checklistUserEntry._ID, id);
                        ModCheckValues.put(checklistContract.checklistUserEntry.USERNAME, usuario);
                        ModCheckValues.put(checklistContract.checklistUserEntry.PASSWORD, senha);
                        ModCheckValues.put(checklistContract.checklistUserEntry.EMAIL, email);
                        ModCheckValues.put(checklistContract.checklistUserEntry.NOME, nome);

                        cVVectorModCheck.add(ModCheckValues);

                        if ( cVVectorModCheck.size() > 0 ) {
                            ContentValues[] cvArrayModCheck = new ContentValues[cVVectorModCheck.size()];
                            cVVectorModCheck.toArray(cvArrayModCheck);
                            getApplicationContext().getContentResolver().bulkInsert(checklistContract.checklistUserEntry.CONTENT_URI, cvArrayModCheck);
                        }

                            SharedPreferences pref2 = getApplicationContext().getSharedPreferences("LoggedUser", MODE_PRIVATE);
                            SharedPreferences.Editor editor2 = pref2.edit();

                            editor2.putString("User", emailStr);
                            editor2.putString("Mail", email);

                            editor2.apply();

                        SharedPreferences pref = getApplicationContext().getSharedPreferences("Jalogou", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();

                        editor.putString("Logou", "SIM");

                        editor.apply();
                        return true;
                        } catch (JSONException e) {
                            return false;
                        }


                    }else if(code == 403){
                        return false;
                    }
                    else if(code == 401){
                        return false;
                    }
                    return true;


                }
                catch (java.net.SocketTimeoutException e) {
                    ErrorURL = TRUE;
                    return false;
                }catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }

            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            userLoginTask = null;
            //stop the progress spinner
            showProgress(false);

            if (success) {
                Intent intent = new Intent(MainActivity.this, Checklist.class);
                startActivity(intent);
            } else {
                if (ErrorURL){
                    new MaterialDialog.Builder(MainActivity.this)
                            .title(R.string.server_timeout_title)
                            .content(R.string.server_timeout_desc)
                            .positiveText(R.string.Dialog_OK)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }else {
                    if (IncorrectPass == 2) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .title(R.string.error_invalid_password)
                                .content(R.string.error_pass_2times)
                                .positiveText(R.string.Dialog_OK)
                                .negativeText(R.string.cancelbutton)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.cancel();
                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        IncorrectPass = 0;
                                    }
                                })
                                .show();
                    }
                    // login failure
                    passwordTextView.setError(getString(R.string.incorrect_password));
                    passwordTextView.requestFocus();
                }
            }
        }

        @Override
        protected void onCancelled() {
            userLoginTask = null;
            showProgress(false);
        }
    }
    public  boolean verificaConexao() {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;
        } else {
            conectado = false;
        }
        return conectado;
    }
}