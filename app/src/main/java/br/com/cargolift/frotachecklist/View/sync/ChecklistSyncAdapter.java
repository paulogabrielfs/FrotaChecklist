package br.com.cargolift.frotachecklist.View.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;
import java.util.Vector;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.Checklist;
import br.com.cargolift.frotachecklist.View.MainActivity;
import br.com.cargolift.frotachecklist.View.Settings;
import br.com.cargolift.frotachecklist.View.data.checklistContract;

import static android.content.Context.MODE_PRIVATE;
import static android.util.Base64.encodeToString;
import static java.lang.Boolean.TRUE;


public class ChecklistSyncAdapter extends AbstractThreadedSyncAdapter {

    private static Context mContext;


    private final String LOG_TAG = ChecklistSyncAdapter.class.getSimpleName();
    private static final int SYNC_INTERVAL = 60 * 60;
    private static final int SYNC_FLEXTIME = SYNC_INTERVAL/3;
    private static final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;
    private static final int WEATHER_NOTIFICATION_ID = 3004;
    int i =0;

    private Settings progress = new Settings();

    private static final String[] NOTIFY_CHECKLIST_PROJECTION = new String[] {
            checklistContract.checklistUserEntry._ID,
            checklistContract.checklistUserEntry.USERNAME,
            checklistContract.checklistUserEntry.PASSWORD,
            checklistContract.checklistUserEntry.EMAIL
    };
    // these indices must match the projection
    private static final int INDEX_USER_ID = 0;
    private static final int INDEX_USER_NAME = 1;
    private static final int INDEX_USER_PASS = 2;
    private static final int INDEX_USER_KEY = 3;

    ChecklistSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
    }


    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        Log.d(LOG_TAG, "onPerformSync Called.");

        ProgressBar ProgressBarUI = Settings.mProgress;

        if (ProgressBarUI != null){
            ProgressBarUI.setProgress(0);
        }
        HttpURLConnection urlConnectionMotorista = null;
        HttpURLConnection urlConnectionBem = null;
        HttpURLConnection urlConnectionModCheck = null;
        HttpURLConnection urlConnectionChecklist = null;
        HttpURLConnection urlConnectionModelo = null;
        HttpURLConnection urlConnectionEtapa = null;
        HttpURLConnection urlConnectionServico = null;
        HttpURLConnection urlConnectionFamilia = null;
        HttpURLConnection urlConnectionFuncionario = null;
        BufferedReader readerMotorista = null;
        BufferedReader readerBem = null;
        BufferedReader readerModCheck = null;
        BufferedReader readerChecklist = null;
        BufferedReader readerModelo = null;
        BufferedReader readerEtapa = null;
        BufferedReader readerServico = null;
        BufferedReader readerFamilia = null;
        BufferedReader readerFuncionario = null;

// Will contain the raw JSON response as a string.

        String checklistJsonStrMotorista = null;

        String checklistJsonStrBem = null;

        String checklistJsonStrModCheck = null;

        String checklistJsonStrChecklist = null;

        String checklistJsonStrModelo = null;

        String checklistJsonStrEtapa = null;

        String checklistJsonStrServico = null;

        String checklistJsonStrFamilia = null;

        String checklistJsonStrFuncionario = null;

        String URL_PADRAO = "http://ws.cargolift.com.br:11605/rest/REST002?cTipo=";
        String name = "webservice";
        String password = "*3uXagUwEWAb";
        String authString = name + ":" + password;
        String authStringEnc = encodeToString(authString.getBytes(), Base64.DEFAULT);
        System.out.println("Base64 encoded auth string: " + authStringEnc);
        // Will contain the raw JSON response as a string.


        try {
            // Construct the URL for the OpenchecklistMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openchecklistmap.org/API#forecast
            URL url2 = new URL(URL_PADRAO + "motorista");
            URL url3 = new URL(URL_PADRAO + "veiculo");
            URL url4 = new URL(URL_PADRAO + "ModCheck");
            URL url5 = new URL(URL_PADRAO + "Check");
            URL url6 = new URL(URL_PADRAO + "modelo");
            URL url7 = new URL(URL_PADRAO + "etapa");
            URL url8 = new URL(URL_PADRAO + "servico");
            URL url9 = new URL(URL_PADRAO + "familia");
            URL url10 = new URL(URL_PADRAO + "funcionario");



            urlConnectionMotorista = (HttpURLConnection) url2.openConnection();
            urlConnectionMotorista.setRequestProperty("Content-Type", "application/json");
            urlConnectionMotorista.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionMotorista.setRequestProperty("connection", "close");
            urlConnectionMotorista.setRequestMethod("GET");
            urlConnectionMotorista.connect();


            InputStream inputStreamMotorista = urlConnectionMotorista.getInputStream();
            StringBuffer bufferMotorista = new StringBuffer();
            if (inputStreamMotorista == null) {
                // Nothing to do.
                return;
            }

            readerMotorista = new BufferedReader(new InputStreamReader(inputStreamMotorista));

            String lineMotorista;
            while ((lineMotorista = readerMotorista.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferMotorista.append(lineMotorista + "\n");
            }

            if (bufferMotorista.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrMotorista = bufferMotorista.toString();

            getChecklistDataFromJsonMotorista(checklistJsonStrMotorista);
            urlConnectionMotorista.disconnect();
            if (ProgressBarUI != null) {
                for (i = 13; i < 24; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }




            urlConnectionBem = (HttpURLConnection) url3.openConnection();
            urlConnectionBem.setRequestProperty("Content-Type", "application/json");
            urlConnectionBem.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionBem.setRequestProperty("connection", "close");
            urlConnectionBem.setRequestMethod("GET");
            urlConnectionBem.connect();

            InputStream inputStreamBem = urlConnectionBem.getInputStream();
            StringBuffer bufferBem  = new StringBuffer();
            if (inputStreamBem == null) {
                // Nothing to do.
                return;
            }

            readerBem = new BufferedReader(new InputStreamReader(inputStreamBem));

            String lineBem;
            while ((lineBem = readerBem.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferBem.append(lineBem + "\n");
            }

            if (bufferBem.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrBem = bufferBem.toString();

            getChecklistDataFromJsonBem(checklistJsonStrBem);
            urlConnectionBem.disconnect();
            if (ProgressBarUI != null) {
                for(i=25; i < 36; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }






            urlConnectionModCheck = (HttpURLConnection) url4.openConnection();
            urlConnectionModCheck.setRequestProperty("Content-Type", "application/json");
            urlConnectionModCheck.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionModCheck.setRequestProperty("connection", "close");
            urlConnectionModCheck.setRequestMethod("GET");
            urlConnectionModCheck.connect();


            InputStream inputStreamModCheck = urlConnectionModCheck.getInputStream();
            StringBuffer bufferModCheck = new StringBuffer();
            if (inputStreamModCheck == null) {
                // Nothing to do.
                return;
            }

            readerModCheck = new BufferedReader(new InputStreamReader(inputStreamModCheck));

            String lineModCheck;
            while ((lineModCheck = readerModCheck.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferModCheck.append(lineModCheck + "\n");
            }

            if (bufferModCheck.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrModCheck = bufferModCheck.toString();

            getChecklistDataFromJsonModCheck(checklistJsonStrModCheck);
            urlConnectionModCheck.disconnect();
            if (ProgressBarUI != null) {
                for (i = 37; i < 48; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }






            urlConnectionChecklist = (HttpURLConnection) url5.openConnection();
            urlConnectionChecklist.setRequestProperty("Content-Type", "application/json");
            urlConnectionChecklist.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionChecklist.setRequestProperty("connection", "close");
            urlConnectionChecklist.setRequestMethod("GET");
            urlConnectionChecklist.connect();

            InputStream inputStreamChecklist = urlConnectionChecklist.getInputStream();
            StringBuffer bufferChecklist = new StringBuffer();
            if (inputStreamChecklist == null) {
                // Nothing to do.
                return;
            }

            readerChecklist = new BufferedReader(new InputStreamReader(inputStreamChecklist));

            String lineChecklist;
            while ((lineChecklist = readerChecklist.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferChecklist.append(lineChecklist + "\n");
            }

            if (bufferChecklist.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrChecklist = bufferChecklist.toString();

            getChecklistDataFromJsonChecklist(checklistJsonStrChecklist);
            urlConnectionChecklist.disconnect();
            if (ProgressBarUI != null) {
                for (i = 49; i < 60; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }







            urlConnectionModelo = (HttpURLConnection) url6.openConnection();
            urlConnectionModelo.setRequestProperty("Content-Type", "application/json");
            urlConnectionModelo.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionModelo.setRequestProperty("connection", "close");
            urlConnectionModelo.setRequestMethod("GET");
            urlConnectionModelo.connect();

            InputStream inputStreamModelo = urlConnectionModelo.getInputStream();
            StringBuffer bufferModelo = new StringBuffer();
            if (inputStreamModelo == null) {
                // Nothing to do.
                return;
            }

            readerModelo = new BufferedReader(new InputStreamReader(inputStreamModelo));

            String lineModelo;
            while ((lineModelo = readerModelo.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferModelo.append(lineModelo + "\n");
            }

            if (bufferModelo.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrModelo = bufferModelo.toString();

            getChecklistDataFromJsonModelo(checklistJsonStrModelo);
            urlConnectionModelo.disconnect();
            if (ProgressBarUI != null) {
                for (i = 61; i < 72; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }





            urlConnectionEtapa = (HttpURLConnection) url7.openConnection();
            urlConnectionEtapa.setRequestProperty("Content-Type", "application/json");
            urlConnectionEtapa.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionEtapa.setRequestProperty("connection", "close");
            urlConnectionEtapa.setRequestMethod("GET");
            urlConnectionEtapa.connect();

            InputStream inputStreamEtapa = urlConnectionEtapa.getInputStream();
            StringBuffer bufferEtapa = new StringBuffer();
            if (inputStreamEtapa == null) {
                // Nothing to do.
                return;
            }

            readerEtapa = new BufferedReader(new InputStreamReader(inputStreamEtapa));

            String lineEtapa;
            while ((lineEtapa = readerEtapa.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferEtapa.append(lineEtapa + "\n");
            }

            if (bufferEtapa.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrEtapa = bufferEtapa.toString();

            getChecklistDataFromJsonEtapa(checklistJsonStrEtapa);
            urlConnectionEtapa.disconnect();
            if (ProgressBarUI != null) {
                for (i = 73; i < 84; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }




            urlConnectionServico = (HttpURLConnection) url8.openConnection();
            urlConnectionServico.setRequestProperty("Content-Type", "application/json");
            urlConnectionServico.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionServico.setRequestProperty("connection", "close");
            urlConnectionServico.setRequestMethod("GET");
            urlConnectionServico.connect();

            InputStream inputStreamServico = urlConnectionServico.getInputStream();
            StringBuffer bufferServico = new StringBuffer();
            if (inputStreamServico == null) {
                // Nothing to do.
                return;
            }

            readerServico = new BufferedReader(new InputStreamReader(inputStreamServico));

            String lineServico;
            while ((lineServico = readerServico.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferServico.append(lineServico + "\n");
            }

            if (bufferServico.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrServico = bufferServico.toString();

            getChecklistDataFromJsonServico(checklistJsonStrServico);
            urlConnectionServico.disconnect();
            if (ProgressBarUI != null) {
                for (i = 85; i < 95; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }

            urlConnectionFuncionario = (HttpURLConnection) url10.openConnection();
            urlConnectionFuncionario.setRequestProperty("Content-Type", "application/json");
            urlConnectionFuncionario.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionFuncionario.setRequestProperty("connection", "close");
            urlConnectionFuncionario.setRequestMethod("GET");
            urlConnectionFuncionario.connect();

            InputStream inputStreamFuncionario = urlConnectionFuncionario.getInputStream();
            StringBuffer bufferFuncionario = new StringBuffer();
            if (inputStreamFuncionario == null) {
                // Nothing to do.
                return;
            }

            readerFuncionario = new BufferedReader(new InputStreamReader(inputStreamFuncionario));

            String lineFuncionario;
            while ((lineFuncionario = readerFuncionario.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferFuncionario.append(lineFuncionario + "\n");
            }

            if (bufferFuncionario.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrFuncionario = bufferFuncionario.toString();

            getChecklistDataFromJsonFuncionario(checklistJsonStrFuncionario);
            urlConnectionFuncionario.disconnect();
            if (ProgressBarUI != null) {
                for (i = 85; i < 95; i++) {
                    ProgressBarUI.setProgress(i);
                }
            }








            urlConnectionFamilia = (HttpURLConnection) url9.openConnection();
            urlConnectionFamilia.setRequestProperty("Content-Type", "application/json");
            urlConnectionFamilia.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
            urlConnectionFamilia.setRequestProperty("connection", "close");
            urlConnectionFamilia.setRequestMethod("GET");
            urlConnectionFamilia.connect();



            InputStream inputStreamFamilia = urlConnectionFamilia.getInputStream();
            StringBuffer bufferFamilia = new StringBuffer();
            if (inputStreamFamilia == null) {
                // Nothing to do.
                return;
            }


            readerFamilia = new BufferedReader(new InputStreamReader(inputStreamFamilia));

            String lineFamilia;
            while ((lineFamilia = readerFamilia.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                bufferFamilia.append(lineFamilia + "\n");
            }

            if (bufferFamilia.length() == 0) {
                // Stream was empty.  No point in parsing.
                return;
            }

            checklistJsonStrFamilia = bufferFamilia.toString();

            getChecklistDataFromJsonFamilia(checklistJsonStrFamilia);
            urlConnectionFamilia.disconnect();
            if (ProgressBarUI != null) {
                for (i = 95; i < 101; i++) {
                    ProgressBarUI.setProgress(i);
                    if (i == 100) {
                        SharedPreferences pref = mContext.getSharedPreferences("GETBACK", MODE_PRIVATE);
                        String TableVersion = pref.getString("VOLTAR", "SIM");
                        if (Objects.equals(TableVersion, "SIM")){
                            Intent intent = new Intent(mContext, Checklist.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            SharedPreferences.Editor editor = pref.edit();

                            editor.putString("VOLTAR", "NAO");

                            editor.apply();
                            mContext.startActivity(intent);
                        }
                    }
                }
            }
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yy.M.dd.hmss");
            final String formattedDate = df.format(c.getTime());

            SharedPreferences pref = mContext.getSharedPreferences("MyPref", MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.putString("key_name5", formattedDate);

            editor.commit();
            if (progress != null) {
                progress.SetarProgresso(true, formattedDate);
            }




        } catch (IOException e) {
            Log.e("PlaceholderFragment", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (readerBem != null) {
                try {
                    readerBem.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }else if (readerModCheck != null) {
                try {
                    readerModCheck.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }else if (readerChecklist != null) {
                try {
                    readerChecklist.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }else if (readerModelo != null) {
                try {
                    readerModelo.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }else if (readerEtapa != null) {
                try {
                    readerEtapa.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }else if (readerServico != null) {
                try {
                    readerServico.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }else if (readerFamilia != null) {
                try {
                    readerFamilia.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }
        return;

    }
    private Void
    getChecklistDataFromJsonMotorista(String checklistJsonStrMotorista)
            throws JSONException {
        final String OWM_cList = "ListMotorista";
        final String OWM_cNome = "Nome";
        final String OWM_cCPF = "CPF";

        try {
            JSONObject checklistJson = new JSONObject(checklistJsonStrMotorista);
            JSONArray checklistArray = checklistJson.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorMotorista = new Vector<ContentValues>(checklistArray.length());


            for(int i = 0; i < checklistArray.length(); i++) {

                String nome;
                String cpf;


                JSONObject MotoristaList = checklistArray.getJSONObject(i);

                nome = MotoristaList.getString(OWM_cNome);
                cpf = MotoristaList.getString(OWM_cCPF);



                ContentValues MotoristaTableValues = new ContentValues();

                MotoristaTableValues.put(checklistContract.MotoristaEntry.COLUMN_NOME, nome);
                MotoristaTableValues.put(checklistContract.MotoristaEntry.COLUMN_CGC, cpf);

                cVVectorMotorista.add(MotoristaTableValues);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorMotorista.size() > 0 ) {
                ContentValues[] cvArrayMotorista = new ContentValues[cVVectorMotorista.size()];
                cVVectorMotorista.toArray(cvArrayMotorista);

                getContext().getContentResolver().delete(checklistContract.MotoristaEntry.CONTENT_URI,
                        checklistContract.MotoristaEntry._ID + " > ?", new String[]{"0"});

                getContext().getContentResolver().bulkInsert(checklistContract.MotoristaEntry.CONTENT_URI, cvArrayMotorista);



                notifyWeather();
            }

            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorMotorista.size() + " Inserted");

        }catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonBem(String checklistJsonStrBem)
            throws JSONException {
        final String OWM_cList = "ListVeiculos";
        final String OWM_cCodBem = "Bem";
        final String OWM_cDescbem = "Descricao";
        final String OWM_cPlaca = "Placa";
        final String OWM_cFamilia = "Familia";
        final String OWM_cTipoModelo = "TipoModelo";
        final String OWM_cSitBem = "SitBem";
        final String OWM_cFabrica = "Fabrica";

        try {
            JSONObject checklistJsonBem = new JSONObject(checklistJsonStrBem);
            JSONArray checklistArrayBem = checklistJsonBem.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorBem = new Vector<ContentValues>(checklistArrayBem.length());


            for(int i = 0; i < checklistArrayBem.length(); i++) {

                String Bem;
                String Descricao;
                String Placa;
                String Familia;
                String TipoModelo;
                String SitBem;
                String Fabrica;


                JSONObject BemList = checklistArrayBem.getJSONObject(i);

                Bem = BemList.getString(OWM_cCodBem);
                Descricao = BemList.getString(OWM_cDescbem);
                Placa = BemList.getString(OWM_cPlaca);
                Familia = BemList.getString(OWM_cFamilia);
                TipoModelo = BemList.getString(OWM_cTipoModelo);
                SitBem = BemList.getString(OWM_cSitBem);
                Fabrica = BemList.getString(OWM_cFabrica);



                ContentValues BemtValues = new ContentValues();

                BemtValues.put(checklistContract.BemEntry.COLUMN_CODBEM, Bem);
                BemtValues.put(checklistContract.BemEntry.COLUMN_NOME, Descricao);
                BemtValues.put(checklistContract.BemEntry.COLUMN_PLACA, Placa);
                BemtValues.put(checklistContract.BemEntry.FK_CODFAM, Familia);
                BemtValues.put(checklistContract.BemEntry.FK_TIPMOD, TipoModelo);
                BemtValues.put(checklistContract.BemEntry.COLUMN_FABRICA, Fabrica);
                BemtValues.put(checklistContract.BemEntry.COLUMN_SITBEM, SitBem);

                cVVectorBem.add(BemtValues);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorBem.size() > 0 ) {
                ContentValues[] cvArrayBem = new ContentValues[cVVectorBem.size()];
                cVVectorBem.toArray(cvArrayBem);


                getContext().getContentResolver().delete(checklistContract.BemEntry.CONTENT_URI,
                        checklistContract.BemEntry._ID + " > ?", new String[]{"0"});
                getContext().getContentResolver().bulkInsert(checklistContract.BemEntry.CONTENT_URI, cvArrayBem);


                notifyWeather();
            }

            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorBem.size() + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonModCheck(String checklistJsonStrModCheck)
            throws JSONException {
        final String OWM_cList = "ListModCheck";
        final String OWM_cSeqFam = "SeqFam";
        final String OWM_cNomSeq = "NomSeq";
        final String OWM_cNumSeq = "NumSeq";
        final String OWM_cTpChkl = "TpChkl";
        final String OWM_cMdChkl = "MdChkl";
        final String OWM_cCodFam = "CodFam";
        final String OWM_cTipMod = "TipMod";

        try {
            JSONObject checklistJsonModCheck = new JSONObject(checklistJsonStrModCheck);
            JSONArray checklistArrayModCheck = checklistJsonModCheck.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorModCheck = new Vector<ContentValues>(checklistArrayModCheck.length());


            for(int i = 0; i < checklistArrayModCheck.length(); i++) {

                String SeqFamilia;
                String NomSeq;
                String NumSeq;
                String TpChkl;
                String ModChkl;
                String CodFam;
                String TipMod;



                JSONObject ModCheckrList = checklistArrayModCheck.getJSONObject(i);

                SeqFamilia = ModCheckrList.getString(OWM_cSeqFam);
                NomSeq = ModCheckrList.getString(OWM_cNomSeq);
                NumSeq = ModCheckrList.getString(OWM_cNumSeq);
                TpChkl = ModCheckrList.getString(OWM_cTpChkl);
                ModChkl = ModCheckrList.getString(OWM_cMdChkl);
                CodFam = ModCheckrList.getString(OWM_cCodFam);
                TipMod = ModCheckrList.getString(OWM_cTipMod);



                ContentValues ModCheckValues = new ContentValues();

                ModCheckValues.put(checklistContract.ModCheckEntry.COLUMN_SEQFAM, SeqFamilia);
                ModCheckValues.put(checklistContract.ModCheckEntry.COLUMN_NOMSEQ, NomSeq);
                ModCheckValues.put(checklistContract.ModCheckEntry.COLUMN_NUMSEQ, NumSeq);
                ModCheckValues.put(checklistContract.ModCheckEntry.COLUMN_TPCHKL, TpChkl);
                ModCheckValues.put(checklistContract.ModCheckEntry.COLUMN_MDCHKL, ModChkl);
                ModCheckValues.put(checklistContract.ModCheckEntry.FK_CODFAM, CodFam);
                ModCheckValues.put(checklistContract.ModCheckEntry.FK_TIPMOD, TipMod);

                cVVectorModCheck.add(ModCheckValues);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorModCheck.size() > 0 ) {
                ContentValues[] cvArrayModCheck = new ContentValues[cVVectorModCheck.size()];
                cVVectorModCheck.toArray(cvArrayModCheck);

                getContext().getContentResolver().delete(checklistContract.ModCheckEntry.CONTENT_URI,
                        checklistContract.ModCheckEntry._ID + " > ?", new String[]{"0"});

                getContext().getContentResolver().bulkInsert(checklistContract.ModCheckEntry.CONTENT_URI, cvArrayModCheck);


                notifyWeather();
            }

            Log.d(LOG_TAG, "Task Complete. " + cVVectorModCheck.size() + " Checklist Models Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonChecklist(String checklistJsonStrChecklist)
            throws JSONException {

        final String OWM_cList = "ListCheck";
        final String OWM_cAlta = "Alta";
        final String OWM_cMedia = "Media";
        final String OWM_cBaixa = "Baixa";
        final String OWM_cServic = "Servic";
        final String OWM_cSeqFam = "SeqFam";
        final String OWM_cCodFam = "CodFam";
        final String OWM_cEtapa = "Etapa";
        final String OWM_cTipMod = "TipMod";

        try {
            JSONObject checklistJson = new JSONObject(checklistJsonStrChecklist);
            JSONArray checklistArray = checklistJson.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorChecklist = new Vector<ContentValues>(checklistArray.length());


            for(int i = 0; i < checklistArray.length(); i++) {

                String Alta;
                String Media;
                String Baixa;
                String Servico;
                String SeqFam;
                String CodFamilia;
                String Etapa;
                String Modelo;


                JSONObject CheckList = checklistArray.getJSONObject(i);

                Alta = CheckList.getString(OWM_cAlta);
                Media = CheckList.getString(OWM_cMedia);
                Baixa = CheckList.getString(OWM_cBaixa);
                Servico = CheckList.getString(OWM_cServic);
                SeqFam = CheckList.getString(OWM_cSeqFam);
                CodFamilia = CheckList.getString(OWM_cCodFam);
                Etapa = CheckList.getString(OWM_cEtapa);
                Modelo = CheckList.getString(OWM_cTipMod);



                ContentValues checklistValues = new ContentValues();

                checklistValues.put(checklistContract.ChecklistkEntry.COLUMN_ALTA, Alta);
                checklistValues.put(checklistContract.ChecklistkEntry.COLUMN_MEDIA, Media);
                checklistValues.put(checklistContract.ChecklistkEntry.COLUMN_BAIXA, Baixa);
                checklistValues.put(checklistContract.ChecklistkEntry.FK_SERVIC, Servico);
                checklistValues.put(checklistContract.ChecklistkEntry.COLUMN_SEQFAM, SeqFam);
                checklistValues.put(checklistContract.ChecklistkEntry.FK_CODFAM, CodFamilia);
                checklistValues.put(checklistContract.ChecklistkEntry.FK_ETAPA, Etapa);
                checklistValues.put(checklistContract.ChecklistkEntry.FK_TIPMOD, Modelo);

                cVVectorChecklist.add(checklistValues);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorChecklist.size() > 0 ) {
                ContentValues[] cvArrayChecklist = new ContentValues[cVVectorChecklist.size()];
                cVVectorChecklist.toArray(cvArrayChecklist);

                getContext().getContentResolver().delete(checklistContract.ChecklistkEntry.CONTENT_URI,
                        checklistContract.ChecklistkEntry._ID + " > ?", new String[]{"0"});

                getContext().getContentResolver().bulkInsert(checklistContract.ChecklistkEntry.CONTENT_URI, cvArrayChecklist);


                notifyWeather();
            }

            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorChecklist.size() + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonModelo(String checklistJsonStrModelo)
            throws JSONException {
        final String OWM_cList = "ListModelo";
        final String OWM_cTpModelo = "TpModelo";
        final String OWM_cDescModelo = "DescModelo";

        try {
            JSONObject checklistJsonModelo = new JSONObject(checklistJsonStrModelo);
            JSONArray checklistArrayModelo = checklistJsonModelo.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorModelo = new Vector<ContentValues>(checklistArrayModelo.length());


            for(int i = 0; i < checklistArrayModelo.length(); i++) {

                String TipoModelo;
                String DescricaoModelo;


                JSONObject ModeloList = checklistArrayModelo.getJSONObject(i);

                TipoModelo = ModeloList.getString(OWM_cTpModelo);
                DescricaoModelo = ModeloList.getString(OWM_cDescModelo);



                ContentValues checklistModeloValues = new ContentValues();

                checklistModeloValues.put(checklistContract.ModeloEntry.COLUMN_TIPMOD, TipoModelo);
                checklistModeloValues.put(checklistContract.ModeloEntry.COLUMN_DESMOD, DescricaoModelo);

                cVVectorModelo.add(checklistModeloValues);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorModelo.size() > 0 ) {
                ContentValues[] cvArrayModelo = new ContentValues[cVVectorModelo.size()];
                cVVectorModelo.toArray(cvArrayModelo);

                getContext().getContentResolver().delete(checklistContract.ModeloEntry.CONTENT_URI,
                        checklistContract.ModeloEntry._ID + " > ?", new String[]{"0"});


                getContext().getContentResolver().bulkInsert(checklistContract.ModeloEntry.CONTENT_URI, cvArrayModelo);


                notifyWeather();
            }

            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorModelo.size() + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonEtapa(String checklistJsonStrEtapa)
            throws JSONException {
        final String OWM_cList = "ListEtapa";
        final String OWM_cIdEtapa = "etapa";
        final String OWM_cDescEtapa = "DescEtapa";

        try {
            JSONObject checklistJsonEtapa = new JSONObject(checklistJsonStrEtapa);
            JSONArray checklistArrayEtapa = checklistJsonEtapa.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorEtapa = new Vector<ContentValues>(checklistArrayEtapa.length());


            for(int i = 0; i < checklistArrayEtapa.length(); i++) {

                String IdEtapa;
                String DescricaoEtapa;


                JSONObject EtapaList = checklistArrayEtapa.getJSONObject(i);

                IdEtapa = EtapaList.getString(OWM_cIdEtapa);
                DescricaoEtapa = EtapaList.getString(OWM_cDescEtapa);



                ContentValues checklistValuesEtapa = new ContentValues();

                checklistValuesEtapa.put(checklistContract.EtapaEntry.COLUMN_ETAPA, IdEtapa);
                checklistValuesEtapa.put(checklistContract.EtapaEntry.COLUMN_DESCRI, DescricaoEtapa);

                cVVectorEtapa.add(checklistValuesEtapa);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorEtapa.size() > 0 ) {
                ContentValues[] cvArrayEtapa = new ContentValues[cVVectorEtapa.size()];
                cVVectorEtapa.toArray(cvArrayEtapa);

                getContext().getContentResolver().delete(checklistContract.EtapaEntry.CONTENT_URI,
                        checklistContract.EtapaEntry._ID + " > ?", new String[]{"0"});


                getContext().getContentResolver().bulkInsert(checklistContract.EtapaEntry.CONTENT_URI, cvArrayEtapa);


                notifyWeather();
            }

            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorEtapa.size() + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonServico(String checklistJsonStrServico)
            throws JSONException {
        final String OWM_cList = "ListServico";
        final String OWM_cIdServico = "servico";
        final String OWM_cDescServico = "DescServico";

        try {
            JSONObject checklistJsonServico = new JSONObject(checklistJsonStrServico);
            JSONArray checklistArrayServico = checklistJsonServico.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorServico = new Vector<ContentValues>(checklistArrayServico.length());


            for(int i = 0; i < checklistArrayServico.length(); i++) {

                String IdServico;
                String DescServico;


                JSONObject ServicoList = checklistArrayServico.getJSONObject(i);

                IdServico = ServicoList.getString(OWM_cIdServico);
                DescServico = ServicoList.getString(OWM_cDescServico);



                ContentValues checklistValuesServico = new ContentValues();

                checklistValuesServico.put(checklistContract.ServicoEntry.COLUMN_SERVIC, IdServico);
                checklistValuesServico.put(checklistContract.ServicoEntry.COLUMN_NOME, DescServico);

                cVVectorServico.add(checklistValuesServico);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorServico.size() > 0 ) {
                ContentValues[] cvArrayServico = new ContentValues[cVVectorServico.size()];
                cVVectorServico.toArray(cvArrayServico);

                getContext().getContentResolver().delete(checklistContract.ServicoEntry.CONTENT_URI,
                        checklistContract.ServicoEntry._ID + " > ?", new String[]{"0"});

                getContext().getContentResolver().bulkInsert(checklistContract.ServicoEntry.CONTENT_URI, cvArrayServico);


                notifyWeather();
            }

            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorServico.size() + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonFuncionario(String checklistJsonStrFuncionario)
            throws JSONException {
        final String OWM_cList = "ListFuncionario";
        final String OWM_cNome = "NOME";
        final String OWM_cEmail = "EMAIL";

        try {
            JSONObject checklistJsonFuncionario = new JSONObject(checklistJsonStrFuncionario);
            JSONArray checklistArrayFuncionario = checklistJsonFuncionario.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorFuncionario = new Vector<ContentValues>(checklistArrayFuncionario.length());


            for(int i = 0; i < checklistArrayFuncionario.length(); i++) {

                String NOME;
                String EMAIL;


                JSONObject FuncionarioList = checklistArrayFuncionario.getJSONObject(i);

                NOME = FuncionarioList.getString(OWM_cNome);
                EMAIL = FuncionarioList.getString(OWM_cEmail);



                ContentValues checklistValuesFuncionario = new ContentValues();

                checklistValuesFuncionario.put(checklistContract.FuncionarioEntry.COLUMN_NOME, NOME);
                checklistValuesFuncionario.put(checklistContract.FuncionarioEntry.COLUMN_EMAIL, EMAIL);

                cVVectorFuncionario.add(checklistValuesFuncionario);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorFuncionario.size() > 0 ) {
                ContentValues[] cvArrayFuncionario = new ContentValues[cVVectorFuncionario.size()];
                cVVectorFuncionario.toArray(cvArrayFuncionario);

                getContext().getContentResolver().delete(checklistContract.FuncionarioEntry.CONTENT_URI,
                        checklistContract.FuncionarioEntry._ID + " > ?", new String[]{"0"});

                getContext().getContentResolver().bulkInsert(checklistContract.FuncionarioEntry.CONTENT_URI, cvArrayFuncionario);


                notifyWeather();
            }
            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorFuncionario.size() + " Inserted");
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }
    private Void
    getChecklistDataFromJsonFamilia(String checklistJsonStrFamilia)
            throws JSONException {
        final String OWM_cList = "ListFamilia";
        final String OWM_cIdFamilia = "Familia";
        final String OWM_cDescFamilia = "DescFamilia";

        try {
            JSONObject checklistJsonFamilia = new JSONObject(checklistJsonStrFamilia);
            JSONArray checklistArrayFamilia = checklistJsonFamilia.getJSONArray(OWM_cList);


            Vector<ContentValues> cVVectorFamilia = new Vector<ContentValues>(checklistArrayFamilia.length());


            for(int i = 0; i < checklistArrayFamilia.length(); i++) {

                String IdFamilia;
                String DescFamilia;



                JSONObject FamiliaList = checklistArrayFamilia.getJSONObject(i);

                IdFamilia = FamiliaList.getString(OWM_cIdFamilia);
                DescFamilia = FamiliaList.getString(OWM_cDescFamilia);



                ContentValues checklistValuesFamilia = new ContentValues();

                checklistValuesFamilia.put(checklistContract.FamiliaEntry.COLUMN_CODFAM, IdFamilia);
                checklistValuesFamilia.put(checklistContract.FamiliaEntry.COLUMN_NOME, DescFamilia);

                cVVectorFamilia.add(checklistValuesFamilia);
            }

            int inserted = 0;
            // add to database
            if ( cVVectorFamilia.size() > 0 ) {
                ContentValues[] cvArrayFamilia = new ContentValues[cVVectorFamilia.size()];
                cVVectorFamilia.toArray(cvArrayFamilia);

                getContext().getContentResolver().delete(checklistContract.FamiliaEntry.CONTENT_URI,
                        checklistContract.FamiliaEntry._ID + " > ?", new String[]{"0"});


                getContext().getContentResolver().bulkInsert(checklistContract.FamiliaEntry.CONTENT_URI, cvArrayFamilia);


                notifyWeather();
            }

            Log.d(LOG_TAG, "FetchWeatherTask Complete. " + cVVectorFamilia.size() + " Inserted");

        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Helper method to have the sync adapter sync immediately
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                context.getString(R.string.content_authority), bundle);
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        if ( null == accountManager.getPassword(newAccount) ) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */

            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }
    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // we can enable inexact timers in our periodic sync
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
        }
    }


    private static void onAccountCreated(Account newAccount, Context context) {
        /*
         * Since we've created an account
         */
        ChecklistSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);

        /*
         * Finally, let's do a sync to get things started
         */
        syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {
    }

    private void notifyWeather() {

        Context context = getContext();
        //checking the last update and notify if it' the first of the day
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String displayNotificationsKey = context.getString(R.string.pref_enable_notifications_key);
        boolean displayNotifications = prefs.getBoolean(displayNotificationsKey,
                Boolean.parseBoolean(context.getString(R.string.pref_enable_notifications_default)));

        if ( displayNotifications ) {

            String lastNotificationKey = context.getString(R.string.pref_last_notification);
        long lastSync = prefs.getLong(lastNotificationKey, 0);

//        if (System.currentTimeMillis() - lastSync >= DAY_IN_MILLIS) {
//            // Last sync was more than 1 day ago, let's send a notification with the weather.
//
//            Uri weatherUri = checklistContract.checklistUserEntry.buildUserUri(1);
//
//            // we'll query our contentProvider, as always
//            Cursor cursor = context.getContentResolver().query(weatherUri, NOTIFY_CHECKLIST_PROJECTION, null, null, "_id ASC");

//            if (cursor.moveToFirst()) {
//                String weatherId = cursor.getString(INDEX_USER_ID);
//                String high = cursor.getString(INDEX_USER_NAME);
//                String low = cursor.getString(INDEX_USER_KEY);
//                String desc = cursor.getString(INDEX_USER_PASS);
//
//                //int iconId = Utility.getIconResourceForWeatherCondition(weatherId);
//                String title = context.getString(R.string.app_name);
//
//                // Define the text of the forecast.
//                String contentText = "Teste";
//                //build your notification here.
//
//                NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getContext())
//                        .setSmallIcon(R.drawable.ic_storm)
//                        .setContentTitle(title)
//                        .setContentText(contentText);
//                Intent resultIntent = new Intent(context, MainActivity.class);
//                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//                stackBuilder.addNextIntent(resultIntent);
//                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//                mBuilder.setContentIntent(resultPendingIntent);
//                //refreshing last sync
//                NotificationManager mNotificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
//
//                mNotificationManager.notify(WEATHER_NOTIFICATION_ID, mBuilder.build());
//
//                SharedPreferences.Editor editor = prefs.edit();
//                editor.putLong(lastNotificationKey, System.currentTimeMillis());
//                editor.commit();
//            }
//        }
        }

    }


}

