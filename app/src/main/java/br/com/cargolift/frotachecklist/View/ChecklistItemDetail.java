package br.com.cargolift.frotachecklist.View;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.media.RatingCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialcamera.MaterialCamera;
import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import br.com.cargolift.frotachecklist.R;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.github.memfis19.annca.Annca;
import io.github.memfis19.annca.internal.configuration.AnncaConfiguration;

import static android.view.View.GONE;
import static java.security.AccessController.getContext;

public class ChecklistItemDetail extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;
    private Bitmap bitmap;
    private ImageView imageView;

    private static final int REQUEST_CAMERA_PERMISSIONS = 931;
    private static final int CAPTURE_MEDIA = 368;

    String Position = null;
    String aprovacao = "true";
    MaterialSpinner spinner;
    String encodedImage = "";

    private Activity activity;

    LinearLayout photo;

    Boolean ProximoPasso = true;
    private final static int CAMERA_RQ = 6969;
    String TextoDescri = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist_item_detail);


        imageView = (ImageView) findViewById(R.id.imgView);

        photo = (LinearLayout) findViewById(R.id.photo);
        final MaterialEditText materialEditText = (MaterialEditText) findViewById(R.id.materialEditText);

        activity = this;

        if (Build.VERSION.SDK_INT > 15) {
            askForPermissions(new String[]{
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.RECORD_AUDIO,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CAMERA_PERMISSIONS);
        } else {
            enableCamera();
        }
        TextView Title = (TextView) findViewById(R.id.EtapaTitle);
        final Button button = (Button) findViewById(R.id.CompletaEtapa);
        final RadioButton AprovadoButton = (RadioButton) findViewById(R.id.ButtonAprovado);
        final RadioButton ReprovadoButton = (RadioButton) findViewById(R.id.ButtonReprovado);
        final RadioButton NaoAplicaButton = (RadioButton) findViewById(R.id.ButtonNaoAplica);

        final MaterialEditText materialedittext = (MaterialEditText) findViewById(R.id.materialEditText);
        final TextView Description_ID = (TextView) findViewById(R.id.Description_ID);

        String Language = Locale.getDefault().toString();

        String[] ITEMS = {"", "", ""};

        if (Objects.equals(Language, "en_US")) {
            ITEMS[0] = "Low";
            ITEMS[1] = "Medium";
            ITEMS[2] = "High";
        } else if (Objects.equals(Language, "es")) {
            ITEMS[0] = "Baja";
            ITEMS[1] = "Media";
            ITEMS[2] = "Alta";
        } else {
            ITEMS[0] = "Baixa";
            ITEMS[1] = "Média";
            ITEMS[2] = "Alta";
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter);

        String Etapa = null;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Position = bundle.getString("Position");
            Etapa = bundle.getString("Etapa");
            Title.setText(Etapa);

        }
        AnncaConfiguration.Builder builder = new AnncaConfiguration.Builder(activity, CAPTURE_MEDIA);

        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(materialEditText.getWindowToken(), 0);

        LinearLayout naoaplica = (LinearLayout) findViewById(R.id.naoaplica);

        if (Objects.equals(Etapa, "KIT CINTAS (2 CATRACAS MOVEL, 20 CINTAS 01 BOLSA)")) {
            naoaplica.setVisibility(View.VISIBLE);
        } else if (Objects.equals(Etapa, "ADESIVOS DE PERSONALIZACAO DA FROTA")) {
                naoaplica.setVisibility(View.VISIBLE);
            }


            final String finalEtapa = Etapa;
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextoDescri = String.valueOf(materialEditText.getText());
                    if (ReprovadoButton.isChecked()) {
                        if (Objects.equals(TextoDescri, "")) {

                            new MaterialDialog.Builder(ChecklistItemDetail.this)
                                    .title(R.string.InformationFault)
                                    .content(R.string.Dialog_One_Text)
                                    .positiveText(R.string.Dialog_OK)
                                    .show();
                            ProximoPasso = false;

                        } else {
                            ProximoPasso = true;
                        }
                        if (imageView.getVisibility() == View.GONE) {
                            new MaterialDialog.Builder(ChecklistItemDetail.this)
                                    .title(R.string.InformationFault)
                                    .content(R.string.Dialog_Two_Text)
                                    .positiveText(R.string.Dialog_OK)
                                    .show();
                            ProximoPasso = false;
                        } else {
                            ProximoPasso = true;
                        }
                        if (ProximoPasso) {
                            Intent returnIntent = new Intent();
                            if (spinner.getSelectedItem() != null) {
                                returnIntent.putExtra("spinner", String.valueOf(spinner.getSelectedItem()));
                            }
                            if (encodedImage != null) {
                                returnIntent.putExtra("encodedImage", encodedImage);
                            }
                            if (!Objects.equals(TextoDescri, "")) {
                                returnIntent.putExtra("TextoDescri", TextoDescri);
                            }
                            returnIntent.putExtra("result", aprovacao);
                            returnIntent.putExtra("position", finalEtapa);

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("ETAPA_REALIZADA", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("TEXTOETAPA", finalEtapa);
                            editor.apply();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }else if (Objects.equals(finalEtapa, "TACOGRAFO / ODOMETRO/ HORIMETRO")) {
                        if (imageView.getVisibility() == View.GONE) {
                            new MaterialDialog.Builder(ChecklistItemDetail.this)
                                    .title(R.string.InformationFault)
                                    .content(R.string.Dialog_Two_Text1)
                                    .positiveText(R.string.Dialog_OK)
                                    .show();
                            ProximoPasso = false;
                        } else {
                            ProximoPasso = true;
                        }
                        if (Objects.equals(TextoDescri, "")) {

                            new MaterialDialog.Builder(ChecklistItemDetail.this)
                                    .title(R.string.InformationFault)
                                    .content(R.string.Dialog_One_Text1)
                                    .positiveText(R.string.Dialog_OK)
                                    .show();
                            ProximoPasso = false;

                        } else {
                            ProximoPasso = true;
                        }
                        if (ProximoPasso) {
                            Intent returnIntent = new Intent();
                            if (spinner.getSelectedItem() != null) {
                                returnIntent.putExtra("spinner", String.valueOf(spinner.getSelectedItem()));
                            }
                            if (encodedImage != null) {
                                returnIntent.putExtra("encodedImage", encodedImage);
                            }
                            if (!Objects.equals(TextoDescri, "")) {
                                returnIntent.putExtra("TextoDescri", TextoDescri);
                            }
                            returnIntent.putExtra("result", aprovacao);
                            returnIntent.putExtra("position", finalEtapa);

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("ETAPA_REALIZADA", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("TEXTOETAPA", finalEtapa);
                            editor.apply();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }else if (Objects.equals(finalEtapa, "HUBODOMETRO")) {
                        if (imageView.getVisibility() == View.GONE) {
                            new MaterialDialog.Builder(ChecklistItemDetail.this)
                                    .title(R.string.InformationFault)
                                    .content(R.string.Dialog_Two_Text1)
                                    .positiveText(R.string.Dialog_OK)
                                    .show();
                            ProximoPasso = false;
                        } else {
                            ProximoPasso = true;
                        }
                        if (Objects.equals(TextoDescri, "")) {

                            new MaterialDialog.Builder(ChecklistItemDetail.this)
                                    .title(R.string.InformationFault)
                                    .content(R.string.Dialog_One_Text1)
                                    .positiveText(R.string.Dialog_OK)
                                    .show();
                            ProximoPasso = false;

                        } else {
                            ProximoPasso = true;
                        }
                        if (ProximoPasso) {
                            Intent returnIntent = new Intent();
                            if (spinner.getSelectedItem() != null) {
                                returnIntent.putExtra("spinner", String.valueOf(spinner.getSelectedItem()));
                            }
                            if (encodedImage != null) {
                                returnIntent.putExtra("encodedImage", encodedImage);
                            }
                            if (!Objects.equals(TextoDescri, "")) {
                                returnIntent.putExtra("TextoDescri", TextoDescri);
                            }
                            returnIntent.putExtra("result", aprovacao);
                            returnIntent.putExtra("position", finalEtapa);

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("ETAPA_REALIZADA", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("TEXTOETAPA", finalEtapa);
                            editor.apply();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                    else {
                        ProximoPasso = true;
                    }
                    if (ProximoPasso) {
                        Intent returnIntent = new Intent();
                        if (spinner.getSelectedItem() != null) {
                            returnIntent.putExtra("spinner", String.valueOf(spinner.getSelectedItem()));
                        }
                        if (encodedImage != null) {
                            returnIntent.putExtra("encodedImage", encodedImage);
                        }
                        returnIntent.putExtra("TextoDescri", TextoDescri);
                        returnIntent.putExtra("result", aprovacao);
                        returnIntent.putExtra("position", finalEtapa);
                        Log.e(Position, "");
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("ETAPA_REALIZADA", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("TEXTOETAPA", finalEtapa);
                        editor.apply();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }
            });

            AprovadoButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (ReprovadoButton.isChecked()) {
                        ReprovadoButton.setChecked(false);
                    }else if (NaoAplicaButton.isChecked()){
                        NaoAplicaButton.setChecked(false);
                    }
                    spinner.setVisibility(GONE);
                    aprovacao = "true";
                }
            });
            ReprovadoButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (AprovadoButton.isChecked()) {
                        AprovadoButton.setChecked(false);
                    }else if (NaoAplicaButton.isChecked()){
                        NaoAplicaButton.setChecked(false);
                    }
                    spinner.setVisibility(View.VISIBLE);
                    aprovacao = "false";
                }
            });
        NaoAplicaButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AprovadoButton.isChecked()) {
                    AprovadoButton.setChecked(false);
                }else if (ReprovadoButton.isChecked()){
                    ReprovadoButton.setChecked(false);
                }
                materialedittext.setVisibility(View.GONE);
                photo.setVisibility(View.GONE);
                Description_ID.setVisibility(View.GONE);
                aprovacao = "true";
            }
        });
        }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new MaterialCamera(ChecklistItemDetail.this)
                    .stillShot()
                    .start(CAMERA_RQ);
        }
    };

    protected final void askForPermissions(String[] permissions, int requestCode) {
        List<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsToRequest.add(permission);
            }
        }
        if (!permissionsToRequest.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissionsToRequest.toArray(new String[permissionsToRequest.size()]), requestCode);
        } else enableCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length == 0) return;
        enableCamera();
    }

    protected void enableCamera() {
        findViewById(R.id.photo).setOnClickListener(onClickListener);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_RQ) {

            if (resultCode == RESULT_OK) {

                photo.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
                String filePath = data.getDataString();
                filePath = filePath.substring(7);
                encodedImage = filePath;
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap Image = BitmapFactory.decodeFile(filePath);

                Bitmap bMapRotate = Bitmap.createBitmap(Image, 0, 0, Image.getWidth(), Image.getHeight(), matrix, true);
                imageView.setImageBitmap(bMapRotate);
                Toast.makeText(this, "Imagem Capturada", Toast.LENGTH_SHORT).show();

            } else if (data != null) {
                Exception e = (Exception) data.getSerializableExtra(MaterialCamera.ERROR_EXTRA);
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }


    }

}