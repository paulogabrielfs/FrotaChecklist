package br.com.cargolift.frotachecklist.View;

import android.Manifest;

import android.app.Activity;

import android.content.ContentValues;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import android.graphics.Bitmap;

import android.graphics.Canvas;

import android.graphics.Color;

import android.net.Uri;

import android.os.Bundle;

import android.os.Environment;

import android.support.annotation.NonNull;

import android.support.v4.app.ActivityCompat;

import android.util.Base64;
import android.util.Log;

import android.view.View;

import android.widget.Button;

import android.widget.Toast;



import com.github.gcacace.signaturepad.views.SignaturePad;


import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.FileOutputStream;

import java.io.IOException;

import java.io.OutputStream;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;


public class SignatureComplete extends Activity {



    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private SignaturePad mSignaturePad;

    private Button mClearButton;

    private Button mSaveButton;

    private String encodedImage;



    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        verifyStoragePermissions(this);

        setContentView(R.layout.activity_signature_complete);



        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);

        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {

            @Override

            public void onStartSigning() {

            }



            @Override

            public void onSigned() {

                mSaveButton.setEnabled(true);

                mClearButton.setEnabled(true);

            }



            @Override

            public void onClear() {

                mSaveButton.setEnabled(false);

                mClearButton.setEnabled(false);

            }

        });



        mClearButton = (Button) findViewById(R.id.clear_button);

        mSaveButton = (Button) findViewById(R.id.save_button);



        mClearButton.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View view) {

                mSignaturePad.clear();

            }

        });



        mSaveButton.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View view) {

                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();

                if (addJpgSignatureToGallery(signatureBitmap)) {

                    Toast.makeText(SignatureComplete.this, "Assinatura salva com sucesso.", Toast.LENGTH_SHORT).show();

                    ContentValues AssiCabecalhoChecklist = new ContentValues();

                    AssiCabecalhoChecklist.put(checklistContract.CabecalhoChecklistEntry.COLUMN_AssMot, encodedImage);

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("InicioChecklist", MODE_PRIVATE);
                    String Data = pref.getString("Data", null);
                    String Hora = pref.getString("Hora", null);

                    String[] mSELECT_TTE_ARGUMENTS = {Data, Hora};

                    getApplicationContext().getContentResolver().update(checklistContract.CabecalhoChecklistEntry.CONTENT_URI, AssiCabecalhoChecklist,
                            checklistContract.CabecalhoChecklistEntry.COLUMN_Data + " = ?" + " AND " + checklistContract.CabecalhoChecklistEntry.COLUMN_Hora + " = ?",mSELECT_TTE_ARGUMENTS);

                    Intent intent = new Intent(SignatureComplete.this, PendentSent.class);
                    startActivity(intent);
                } else {

                    Toast.makeText(SignatureComplete.this, "Falha ao salvar assinatura", Toast.LENGTH_SHORT).show();

                }

            }

        });

    }



    @Override

    public void onRequestPermissionsResult(int requestCode,

                                           @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {

            case REQUEST_EXTERNAL_STORAGE: {

                // If request is cancelled, the result arrays are empty.

                if (grantResults.length <= 0

                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(SignatureComplete.this, "Cannot write images to external storage", Toast.LENGTH_SHORT).show();

                }

            }

        }

    }



    public File getAlbumStorageDir(String albumName) {

        // Get the directory for the user's public pictures directory.

        File file = new File(Environment.getExternalStoragePublicDirectory(

                Environment.DIRECTORY_PICTURES), albumName);

        if (!file.mkdirs()) {

            Log.e("SignaturePad", "Directory not created");

        }

        return file;

    }



    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {

        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(newBitmap);

        canvas.drawColor(Color.WHITE);

        canvas.drawBitmap(bitmap, 0, 0, null);

        OutputStream stream = new FileOutputStream(photo);

        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);

        stream.close();

    }



    public boolean addJpgSignatureToGallery(Bitmap signature) {

        boolean result = false;

        try {

            File photo = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.jpg", System.currentTimeMillis()));

            saveBitmapToJPG(signature, photo);

            scanMediaFile(photo);

            Bitmap bm = signature;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 20, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);

            result = true;

        } catch (IOException e) {

            e.printStackTrace();

        }

        return result;

    }



    private void scanMediaFile(File photo) {

        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);

        Uri contentUri = Uri.fromFile(photo);

        mediaScanIntent.setData(contentUri);

        SignatureComplete.this.sendBroadcast(mediaScanIntent);

    }

    /**

     * Checks if the app has permission to write to device storage

     * <p/>

     * If the app does not has permission then the user will be prompted to grant permissions

     *

     * @param activity the activity from which permissions are checked

     */

    public static void verifyStoragePermissions(Activity activity) {

        // Check if we have write permission

        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);



        if (permission != PackageManager.PERMISSION_GRANTED) {

            // We don't have permission so prompt the user

            ActivityCompat.requestPermissions(

                    activity,

                    PERMISSIONS_STORAGE,

                    REQUEST_EXTERNAL_STORAGE

            );

        }

    }

}