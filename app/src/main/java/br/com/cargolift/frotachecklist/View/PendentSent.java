package br.com.cargolift.frotachecklist.View;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.Vector;
import java.net.URL;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;

import static android.util.Base64.encodeToString;
import static java.lang.Boolean.TRUE;

public class PendentSent extends AppCompatActivity {

    String SelectEtapasWithHoraAndData;

    String[] mSELECT_TTE_ARGUMENTS = {"", ""};

    int CountEnviosPendentes = 0;
    int CountEnviosPendentes2 = 0;
    int CountEnviosPendentesForSent = 0;
    int CountItemPendentesForSent = 0;
    ArrayList<DataModel> dataModels;
    ListView listView;
    private static CabecalhoItemAdapter adapter;


    private static final String[] CABECALHO_COLUMNS = {

            checklistContract.CabecalhoChecklistEntry._ID,
            checklistContract.CabecalhoChecklistEntry.COLUMN_Bem,
            checklistContract.CabecalhoChecklistEntry.COLUMN_Placa,
            checklistContract.CabecalhoChecklistEntry.COLUMN_Familia,
            checklistContract.CabecalhoChecklistEntry.COLUMN_Data,
            checklistContract.CabecalhoChecklistEntry.COLUMN_Hora,
            checklistContract.CabecalhoChecklistEntry.COLUMN_TpCheck,
            checklistContract.CabecalhoChecklistEntry.COLUMN_ModCheck,
            checklistContract.CabecalhoChecklistEntry.COLUMN_SitVeic,
            checklistContract.CabecalhoChecklistEntry.COLUMN_USER,
            checklistContract.CabecalhoChecklistEntry.COLUMN_CPFMot,
            checklistContract.CabecalhoChecklistEntry.COLUMN_AssMot,
            checklistContract.CabecalhoChecklistEntry.COLUMN_TIPMOD
    };


    static final int COLUMN_ID_Cabecalho = 0;
    static final int COLUMN_Bem = 1;
    static final int COLUMN_Placa = 2;
    static final int COLUMN_Familia = 3;
    static final int COLUMN_Data = 4;
    static final int COLUMN_Hora = 5;
    static final int COLUMN_TpCheck = 6;
    static final int COLUMN_ModCheck = 7;
    static final int COLUMN_SitVeic = 8;
    static final int COLUMN_USER = 9;
    static final int COLUMN_CPFMot = 10;
    static final int COLUMN_AssMot = 11;
    static final int COLUMN_TIPMOD = 12;

    private static final String[] Items_COLUMNS = {

            checklistContract.ItemChecklistEntry._ID,
            checklistContract.ItemChecklistEntry.COLUMN_Etapa,
            checklistContract.ItemChecklistEntry.COLUMN_Desc,
            checklistContract.ItemChecklistEntry.COLUMN_Critic,
            checklistContract.ItemChecklistEntry.COLUMN_Img,
            checklistContract.ItemChecklistEntry.COLUMN_TextoComp,
            checklistContract.ItemChecklistEntry.COLUMN_data,
            checklistContract.ItemChecklistEntry.COLUMN_hora,
            checklistContract.ItemChecklistEntry.COLUMN_status
    };


    static final int COLUMN_ID_Etapa = 0;
    static final int COLUMN_Etapa = 1;
    static final int COLUMN_Desc = 2;
    static final int COLUMN_Critic = 3;
    static final int COLUMN_Img = 4;
    static final int COLUMN_TextoComp = 5;
    static final int COLUMN_data = 6;
    static final int COLUMN_hora = 7;
    static final int COLUMN_status = 8;

    String [] delete = {""};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendent_sent);

        LinearLayout  ZeroItemsLayout = (LinearLayout) findViewById(R.id.ZeroItemsLayout);

        listView = (ListView) findViewById(R.id.list);


        Uri UriPesquisaEnvios = checklistContract.CabecalhoChecklistEntry.buildCabecalhoChecklistUri();



        Button enviartudo = (Button) findViewById(R.id.enviartudo);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Cursor cur2 = getApplicationContext().getContentResolver().query(UriPesquisaEnvios,CABECALHO_COLUMNS, null, null, "_id ASC");
        CountEnviosPendentes2 = cur2.getCount();
        // Get ListView object from xml

        // Defined Array values to show in ListView
        if (CountEnviosPendentes2 > 0){
            ZeroItemsLayout.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            enviartudo.setVisibility(View.VISIBLE);
            dataModels= new ArrayList<>();

            cur2.moveToFirst();


            for (int i = 0; i < CountEnviosPendentes2; i++){
                dataModels.add(new DataModel(cur2.getString(COLUMN_Bem), cur2.getString(COLUMN_Data), cur2.getString(COLUMN_Hora)));
                cur2.moveToNext();
            }


            adapter= new CabecalhoItemAdapter(dataModels,getApplicationContext());

            listView.setAdapter(adapter);

            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, final View view,
                                               final int position, long arg3) {
                    new MaterialDialog.Builder(PendentSent.this)
                            .title(R.string.Delete)
                            .content(R.string.WantToDelete)
                            .positiveText(R.string.Dialog_OK)
                            .negativeText(R.string.cancelbutton)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    TextView dataView = (TextView) view.findViewById(R.id.TVData);
                                    String dataText = dataView.getText().toString();
                                    TextView horaView = (TextView) view.findViewById(R.id.TVHora);
                                    String horaText = horaView.getText().toString();

                                    getApplicationContext().getContentResolver().delete(checklistContract.ItemChecklistEntry.CONTENT_URI,
                                            checklistContract.ItemChecklistEntry.COLUMN_data + " = ? AND " + checklistContract.ItemChecklistEntry.COLUMN_hora + " = ?",
                                            new String[]{dataText, horaText});

                                    getApplicationContext().getContentResolver().delete(checklistContract.CabecalhoChecklistEntry.CONTENT_URI,
                                            checklistContract.CabecalhoChecklistEntry.COLUMN_Data + " = ? AND " + checklistContract.CabecalhoChecklistEntry.COLUMN_Hora + " = ?",
                                            new String[]{dataText, horaText});


                                    dataModels.remove(position);
                                    adapter.notifyDataSetChanged();
                                }
                            })
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.cancel();
                                }
                            })
                            .cancelable(false)
                            .show();
                    return false;
                }

            });

            enviartudo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (verificaConexao()) {
                        new sendPostWithChecklist().execute();
                    }else {
                        new MaterialDialog.Builder(PendentSent.this)
                                .title(R.string.NoConnection)
                                .content(R.string.NeedInternetConnection)
                                .positiveText(R.string.Dialog_OK)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        Intent intent = new Intent(PendentSent.this, Checklist.class);
                                        startActivity(intent);

                                    }
                                })
                                .cancelable(false)
                                .show();
                    }
                }
            });



            //-----------------------------**-----------------------------//




        }

    }
    public  boolean verificaConexao() {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;
        } else {
            conectado = false;
        }
        return conectado;
    }
    public class sendPostWithChecklist extends AsyncTask<Object, Object, String> {
        MaterialDialog.Builder pd;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new MaterialDialog.Builder(PendentSent.this)
                    .title(R.string.SendingChecklist)
                    .content(R.string.PleaseWait)
                    .progress(true, 0)
                    .cancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(Object... params) {

            Uri UriPesquisaEnvios = checklistContract.CabecalhoChecklistEntry.buildCabecalhoChecklistUri();

            Cursor cur = getApplicationContext().getContentResolver().query(UriPesquisaEnvios, CABECALHO_COLUMNS, null, null, "_id ASC");
            CountEnviosPendentesForSent = cur.getCount();
            cur.moveToFirst();
            // create the albums object
            JsonObject checklist = new JsonObject();
            // create an array called datasets
            JsonArray Checklists = new JsonArray();


            JsonObject CabecalhoChecklist = new JsonObject();


            JsonArray Etapas = new JsonArray();

            JsonObject EtapasFromChecklist = new JsonObject();

            Cursor mCursorEtapas = null;
            int code = 0;
            for (int i = 0; i < CountEnviosPendentesForSent; i++) {

                // create the albums object
                checklist = new JsonObject();
                // create an array called datasets
                Checklists = new JsonArray();


                CabecalhoChecklist = new JsonObject();

                CabecalhoChecklist.addProperty("bem", cur.getString(COLUMN_Bem));

                CabecalhoChecklist.addProperty("placa", cur.getString(COLUMN_Placa));

                CabecalhoChecklist.addProperty("familia", cur.getString(COLUMN_Familia));

                CabecalhoChecklist.addProperty("data", cur.getString(COLUMN_Data));

                CabecalhoChecklist.addProperty("hora", cur.getString(COLUMN_Hora));

                CabecalhoChecklist.addProperty("tpcheck", cur.getString(COLUMN_TpCheck));

                CabecalhoChecklist.addProperty("modcheck", cur.getString(COLUMN_ModCheck));

                CabecalhoChecklist.addProperty("sitveic", cur.getString(COLUMN_SitVeic).charAt(0));

                CabecalhoChecklist.addProperty("user", cur.getString(COLUMN_USER));

                CabecalhoChecklist.addProperty("cpfmot", cur.getString(COLUMN_CPFMot));

                CabecalhoChecklist.addProperty("assmot", cur.getString(COLUMN_AssMot));

                CabecalhoChecklist.addProperty("tipomod", cur.getString(COLUMN_TIPMOD));


                if (cur.getString(COLUMN_Data) == null) {
                    SelectEtapasWithHoraAndData = null;
                    mSELECT_TTE_ARGUMENTS[0] = "";
                    mSELECT_TTE_ARGUMENTS[1] = "";

                } else {
                    SelectEtapasWithHoraAndData = checklistContract.ItemChecklistEntry.COLUMN_data + " = ?" + " AND " + checklistContract.ItemChecklistEntry.COLUMN_hora + " = ?";

                    mSELECT_TTE_ARGUMENTS[0] = cur.getString(COLUMN_Data);
                    mSELECT_TTE_ARGUMENTS[1] = cur.getString(COLUMN_Hora);
                }

                Uri BuscaEtapasChecklist = checklistContract.ItemChecklistEntry.buildItemChecklistUri();

                mCursorEtapas = getApplicationContext().getContentResolver().query(
                        BuscaEtapasChecklist, Items_COLUMNS, SelectEtapasWithHoraAndData, mSELECT_TTE_ARGUMENTS, "_ID ASC");

                CountItemPendentesForSent = mCursorEtapas.getCount();
                mCursorEtapas.moveToFirst();

                Etapas = new JsonArray();

                for (int k = 0; k < CountItemPendentesForSent; k++) {


                    EtapasFromChecklist = new JsonObject();

                    String[] delete = {mCursorEtapas.getString(COLUMN_ID_Etapa)};


                    String etapa = "";
                    etapa = mCursorEtapas.getString(COLUMN_Etapa);

                    EtapasFromChecklist.addProperty("etapa", etapa);


                    String Desc = "";
                    Desc = mCursorEtapas.getString(COLUMN_Desc);
                    EtapasFromChecklist.addProperty("desc", Desc);


                    String criticidade = "";
                    criticidade = mCursorEtapas.getString(COLUMN_Critic);
                    String F1Crit = String.valueOf(criticidade.charAt(0));

                    EtapasFromChecklist.addProperty("crit", F1Crit);

                    String encodedImage = "";
                    if (!Objects.equals(mCursorEtapas.getString(COLUMN_Img), "")) {
                        Bitmap bm = BitmapFactory.decodeFile(mCursorEtapas.getString(COLUMN_Img));
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 20, baos); //bm is the bitmap object
                        byte[] b = baos.toByteArray();
                        encodedImage = Base64.encodeToString(b, Base64.NO_WRAP);
                    }

                    EtapasFromChecklist.addProperty("img", encodedImage);
                    String TextoComp;
                    if (!Objects.equals(mCursorEtapas.getString(COLUMN_TextoComp), "")) {
                        TextoComp = mCursorEtapas.getString(COLUMN_TextoComp);
                    } else {
                        TextoComp = "   ";
                    }

                    EtapasFromChecklist.addProperty("textocom", TextoComp);

                    String StatusEtapa;
                    if (!Objects.equals(mCursorEtapas.getString(COLUMN_status), "")) {
                        if (Objects.equals(mCursorEtapas.getString(COLUMN_status), "Aprovado")) {
                            StatusEtapa = "A";
                        } else {
                            StatusEtapa = "R";
                        }
                    } else {
                        StatusEtapa = "   ";
                    }

                    EtapasFromChecklist.addProperty("statusetapa", StatusEtapa);

                    Etapas.add(EtapasFromChecklist);

                    mCursorEtapas.moveToNext();
                }
                cur.moveToNext();
                CabecalhoChecklist.add("etapas", new Gson().toJsonTree(Etapas));
                Checklists.add(CabecalhoChecklist);

                checklist.add("ListChecklist", Checklists);
                Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
                Vector<ContentValues> cVVectorModCheck = new Vector<ContentValues>();
                ContentValues ModCheckValues = new ContentValues();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat hora = new SimpleDateFormat("kk:mm");
                final String dataformatada = data.format(c.getTime());
                final String horafortmatada = hora.format(c.getTime());

                ModCheckValues.put(checklistContract.JsonEntry.COLUMN_JSON, gson.toJson(checklist));
                ModCheckValues.put(checklistContract.JsonEntry.COLUMN_DATA, dataformatada);
                ModCheckValues.put(checklistContract.JsonEntry.COLUMN_HORA, horafortmatada);

                cVVectorModCheck.add(ModCheckValues);

                HttpURLConnection urlConnectionUsers = null;

                URL URL_PADRAO = null;


                ContentValues JsonLog = new ContentValues();

                JsonLog.put(checklistContract.LogEntry.COLUMN_JSONLOG, gson.toJson(checklist));


                getApplicationContext().getContentResolver().insert(checklistContract.LogEntry.CONTENT_URI, JsonLog);

                try {
                    URL_PADRAO = new URL("http://ws.cargolift.com.br:11605/rest/REST002");
                    String name = "webservice";
                    String password = "*3uXagUwEWAb";
                    String authString = name + ":" + password;
                    String authStringEnc = encodeToString(authString.getBytes(), Base64.DEFAULT);

                    urlConnectionUsers = (HttpURLConnection) URL_PADRAO.openConnection();
                    urlConnectionUsers.setDoInput(true);
                    urlConnectionUsers.setDoOutput(true);
                    urlConnectionUsers.setUseCaches(false);
                    urlConnectionUsers.setRequestMethod("POST");
                    urlConnectionUsers.setConnectTimeout(60 * 1000);
                    urlConnectionUsers.setRequestProperty("Content-Type", "application/json");
                    urlConnectionUsers.setRequestProperty("Authorization", "Basic " + authStringEnc.substring(0, authStringEnc.length() - 1));
                    urlConnectionUsers.setRequestProperty("connection", "close");


                    DataOutputStream wr = new DataOutputStream(urlConnectionUsers.getOutputStream());
                    wr.writeBytes(gson.toJson(checklist));


                    wr.flush();
                    wr.close();

//                InputStream is = urlConnectionUsers.getInputStream();
//                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//                String line;
//                StringBuffer response = new StringBuffer();
//                while((line = rd.readLine()) != null) {
//                    response.append(line);
//                    response.append('\r');
//                }

//                rd.close();
                    code = urlConnectionUsers.getResponseCode();
                    if (code == 201) {

                        if (cVVectorModCheck.size() > 0) {
                            ContentValues[] cvArrayModCheck = new ContentValues[cVVectorModCheck.size()];
                            cVVectorModCheck.toArray(cvArrayModCheck);

                            getApplicationContext().getContentResolver().bulkInsert(checklistContract.JsonEntry.CONTENT_URI, cvArrayModCheck);

                        }

                    }
                } catch (java.net.SocketTimeoutException e) {
                } catch (IOException e) {
                    e.printStackTrace();
                }


                int inserted = 0;
                // add to database

                System.out.println(gson.toJson(checklist));
            }

            mCursorEtapas.close();

            cur.close();
            return String.valueOf(code);
        }


        @Override
        protected void onPostExecute(final String success) {
            if (success == null){
                new MaterialDialog.Builder(PendentSent.this)
                        .title(R.string.server_timeout_title)
                        .content(R.string.server_timeout_desc)
                        .positiveText(R.string.Dialog_OK)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }else {
                super.onPostExecute(success);
                getApplicationContext().getContentResolver().delete(checklistContract.CabecalhoChecklistEntry.CONTENT_URI,
                        null, null);
                getApplicationContext().getContentResolver().delete(checklistContract.ItemChecklistEntry.CONTENT_URI,
                        null, null);
                dataModels.clear();
                adapter.notifyDataSetChanged();

                Intent intent = new Intent(PendentSent.this, PendentSent.class);
                startActivity(intent);
            }

        }

        @Override
        protected void onCancelled() {
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), Checklist.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), Checklist.class);
        startActivity(intent);
    }




}

