package br.com.cargolift.frotachecklist.View;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.UserDictionary;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nihaskalam.progressbuttonlibrary.CircularProgressButton;

import java.util.Locale;
import java.util.Objects;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;
import fr.ganfra.materialspinner.MaterialSpinner;
import ru.kolotnev.formattedittext.MaskedEditText;
import tarek360.animated.icons.AnimatedIconView;
import tarek360.animated.icons.IconFactory;

import static android.R.attr.button;
import static android.R.attr.functionalTest;

public class Checklist extends AppCompatActivity {

    String mSearchString = "";
    private String mSelectionClause;
    private Cursor mCursor;

    private static final String[] BEM_COLUMNS = {

            checklistContract.BemEntry._ID,
            checklistContract.BemEntry.FK_CODFAM,
            checklistContract.BemEntry.FK_TIPMOD,
            checklistContract.BemEntry.COLUMN_CODBEM,
            checklistContract.BemEntry.COLUMN_NOME,
            checklistContract.BemEntry.COLUMN_PLACA,
            checklistContract.BemEntry.COLUMN_SITBEM,
            checklistContract.BemEntry.COLUMN_FABRICA
    };

    static final int COL_BEM_ID = 0;
    static final int COL_BEM_FK_CODFAM = 1;
    static final int COL_BEM_FK_TIPMOD = 2;
    static final int COL_BEM_COLUMN_CODBEM = 3;
    static final int COL_BEM_COLUMN_NOME = 4;
    static final int COL_BEM_COLUMN_PLACA = 5;
    static final int COL_BEM_COLUMN_SITBEM = 6;
    static final int COL_BEM_COLUMN_FABRICA = 7;

    static final int COLUMN_ID_Cabecalho = 0;
    static final int COLUMN_Bem = 1;
    static final int COLUMN_Placa = 2;
    static final int COLUMN_Familia = 3;
    static final int COLUMN_Data = 4;
    static final int COLUMN_Hora = 5;
    static final int COLUMN_TpCheck = 6;
    static final int COLUMN_ModCheck = 7;
    static final int COLUMN_SitVeic = 8;
    static final int COLUMN_USER = 9;
    static final int COLUMN_CPFMot = 10;

    int CountEnviosPendentes = 0;
    int CountEnviosPendentes2 = 0;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        final ProgressBar SearchProgress = (ProgressBar) findViewById(R.id.ProgressoBusca);
        final CircularProgressButton BuscaPlaca = (CircularProgressButton) findViewById(R.id.circularButton3);
        final MaskedEditText StringPlaca = (MaskedEditText) findViewById(R.id.StringPlaca);
        final AnimatedIconView ButtonSettings = (AnimatedIconView) findViewById(R.id.ButtonSettings);
        final AnimatedIconView PendentItenButton = (AnimatedIconView) findViewById(R.id.PendentItenButton);
        MaskedEditText MeditText = (MaskedEditText) findViewById(R.id.StringPlaca);
        final LinearLayout toplayout = (LinearLayout) findViewById(R.id.TopLayout);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        Uri UriPesquisaEnvios = checklistContract.CabecalhoChecklistEntry.buildCabecalhoChecklistUri();

        Cursor cur = getApplicationContext().getContentResolver().query(UriPesquisaEnvios, null, null, null, "_id ASC");
        CountEnviosPendentes = cur.getCount();
        cur.moveToFirst();

        for (int ij = 0; ij < CountEnviosPendentes; ij++){
            String Data = cur.getString(COLUMN_Data);
            String Hora = cur.getString(COLUMN_Hora);
            String Situacao = cur.getString(COLUMN_SitVeic);

            if (Objects.equals(Situacao, "")){
                getApplicationContext().getContentResolver().delete(checklistContract.CabecalhoChecklistEntry.CONTENT_URI,
                        checklistContract.CabecalhoChecklistEntry.COLUMN_Data + " = ? AND " + checklistContract.CabecalhoChecklistEntry.COLUMN_Hora + " = ?",
                        new String[]{Data, Hora});
            }

            cur.moveToNext();
        }

        cur = getApplicationContext().getContentResolver().query(UriPesquisaEnvios, null, null, null, "_id ASC");
        CountEnviosPendentes2 = cur.getCount();

        PendentItenButton.setAnimatedIcon(IconFactory.iconNotificationAlert().setNotificationCount(CountEnviosPendentes2));
        PendentItenButton.startAnimation();
        PendentItenButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent iniciaEnvios = new Intent(Checklist.this, PendentSent.class);
                startActivity(iniciaEnvios);
            }

        });
        ButtonSettings.setAnimatedIcon(IconFactory.iconSettings());
        ButtonSettings.startAnimation();
        ButtonSettings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ButtonSettings.startAnimation();
                Intent iniciaconfiguracoes = new Intent(Checklist.this, Settings.class);
                startActivity(iniciaconfiguracoes);
            }

        });
        BuscaPlaca.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(StringPlaca.getWindowToken(), 0);

                mSearchString = String.valueOf(StringPlaca.getText(true));
                mSearchString = mSearchString.toUpperCase();

                if (Objects.equals(mSearchString, "")) {
                    new MaterialDialog.Builder(Checklist.this)
                            .title(R.string.Invalid_Fleet)
                            .content(R.string.Dialog_Tree_Text)
                            .positiveText(R.string.Dialog_OK)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                    dialog.cancel();

                                }
                            })
                            .show();
                } else {


                    BuscaPlaca.setVisibility(View.GONE);
                    SearchProgress.setVisibility(View.VISIBLE);
                    StringPlaca.setInputType(InputType.TYPE_NULL);
                    StringPlaca.setTextIsSelectable(false);
                    StringPlaca.setKeyListener(null);

                    String[] mSelectionArgs = {""};


                    final Uri BuscarPlaca = checklistContract.BemEntry.buildBemUri();

                    if (TextUtils.isEmpty(mSearchString)) {
                        mSelectionClause = null;
                        mSelectionArgs[0] = "";

                    } else {
                        mSelectionClause = checklistContract.BemEntry.COLUMN_CODBEM + " = ?";

                        mSelectionArgs[0] = mSearchString;

                        mCursor = getContentResolver().query(
                                BuscarPlaca, BEM_COLUMNS, mSelectionClause, mSelectionArgs, "_id ASC");

                        mCursor.moveToFirst();

                    }


                    if (null == mCursor) {
                    } else if (mCursor.getCount() < 1) {

                        mSelectionClause = checklistContract.BemEntry.COLUMN_PLACA + " = ?";

                        mSelectionArgs[0] = mSearchString;

                        mCursor = getContentResolver().query(
                                BuscarPlaca, BEM_COLUMNS, mSelectionClause, mSelectionArgs, "_id ASC");

                        mCursor.moveToFirst();

                        if (mCursor.getCount() < 1) {

                            new MaterialDialog.Builder(Checklist.this)
                                    .title(R.string.Register_not_found)
                                    .content(R.string.Dialog_Foure_Text)
                                    .positiveText(R.string.TextView_atualizar)
                                    .negativeText(R.string.cancelbutton)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                            Intent intent = new Intent(Checklist.this, Settings.class);
                                            startActivity(intent);

                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            dialog.cancel();
                                        }
                                    })
                                    .show();

                            StringPlaca.setTextIsSelectable(false);
                            BuscaPlaca.setVisibility(View.VISIBLE);
                            SearchProgress.setVisibility(View.GONE);
                            StringPlaca.setInputType(1);
                            inputMethodManager.showSoftInput(StringPlaca, 0);
                        } else if (Objects.equals(mCursor.getString(COL_BEM_COLUMN_SITBEM), "I")){
                            new MaterialDialog.Builder(Checklist.this)
                                    .title("Bem inativo")
                                    .content("Este BEM está inativo no sistema.")
                                    .positiveText(R.string.Dialog_OK)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                            Intent intent = new Intent(Checklist.this, Checklist.class);
                                            startActivity(intent);

                                        }
                                    })
                                    .show();
                        }else{
                            mSearchString = mCursor.getString(COL_BEM_COLUMN_CODBEM);
                            Intent intent = new Intent(Checklist.this, InicioChecklist.class);
                            intent.putExtra("Placa", mSearchString);
                            startActivity(intent);
                        }

                    } else if (Objects.equals(mCursor.getString(COL_BEM_COLUMN_SITBEM), "I")){
                        new MaterialDialog.Builder(Checklist.this)
                                .title("Bem inativo")
                                .content("Este BEM está inativo no sistema.")
                                .positiveText(R.string.Dialog_OK)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        Intent intent = new Intent(Checklist.this, Checklist.class);
                                        startActivity(intent);

                                    }
                                })
                                .show();
                    }else{
                        Intent intent = new Intent(Checklist.this, InicioChecklist.class);
                        intent.putExtra("Placa", mSearchString);
                        startActivity(intent);
                    }
                }


            }
        });
    }
    @Override
    public void onBackPressed() {

        new MaterialDialog.Builder(Checklist.this)
                .title(R.string.attention)
                .content(R.string.close_Session)
                .positiveText(R.string.Logout)
                .negativeText(R.string.cancelbutton)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);

                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.cancel();
                    }
                })
                .show();
    }
}
