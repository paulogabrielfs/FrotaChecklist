package br.com.cargolift.frotachecklist.View.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import br.com.cargolift.frotachecklist.View.sync.ChecklistSyncAdapter;

public class ChecklistSyncService extends Service {
    private static final Object sSyncAdapterLock = new Object();
    private static ChecklistSyncAdapter sSunshineSyncAdapter = null;

    @Override
    public void onCreate() {
        Log.d("ChecklistSyncService", "onCreate - ChecklistSyncService");
        synchronized (sSyncAdapterLock) {
            if (sSunshineSyncAdapter == null) {
                sSunshineSyncAdapter = new ChecklistSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSunshineSyncAdapter.getSyncAdapterBinder();
    }
}