package br.com.cargolift.frotachecklist.View;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.ByteArrayOutputStream;
import java.util.Objects;

import br.com.cargolift.frotachecklist.R;
import br.com.cargolift.frotachecklist.View.data.checklistContract;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by neokree on 16/12/14.
 */
public class ListaChecklistFragmentReprovado extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final int FORECAST_LOADER = 0;
    private chescklistAdapter mChecklistAdapter;
    int CountCorrectItens;
    int CountIncorrectItens;
    private Uri BuscaEtapasItemChecklist = checklistContract.ItemChecklistEntry.buildItemChecklistUri();

    private static final String[] USERLIST_COLUMNS = {
            checklistContract.checklistUserEntry.TABLE_NAME +"."+
                    checklistContract.checklistUserEntry._ID,
            checklistContract.checklistUserEntry.USERNAME,
            checklistContract.checklistUserEntry.PASSWORD,
            checklistContract.checklistUserEntry.EMAIL
    };
    public static final String[] ETAPA_COLUMNS = {
            checklistContract.EtapaEntry.TABLE_NAME +"."+
                    checklistContract.EtapaEntry._ID,
            checklistContract.EtapaEntry.COLUMN_ETAPA,
            checklistContract.EtapaEntry.COLUMN_DESCRI
    };
    private static final String[] ItemChecklist_COLUMNS = {
            checklistContract.ItemChecklistEntry.TABLE_NAME +"."+
                    checklistContract.ItemChecklistEntry._ID,
            checklistContract.ItemChecklistEntry.COLUMN_Etapa,
            checklistContract.ItemChecklistEntry.COLUMN_Desc,
            checklistContract.ItemChecklistEntry.COLUMN_Critic,
            checklistContract.ItemChecklistEntry.COLUMN_Img,
            checklistContract.ItemChecklistEntry.COLUMN_TextoComp,
            checklistContract.ItemChecklistEntry.COLUMN_data,
            checklistContract.ItemChecklistEntry.COLUMN_hora,
            checklistContract.ItemChecklistEntry.COLUMN_status
    };
    private static final String[] Checklist_COLUMNS = {
            checklistContract.ChecklistkEntry.TABLE_NAME +"."+
                    checklistContract.ChecklistkEntry._ID,
            checklistContract.ChecklistkEntry.FK_ETAPA,
            checklistContract.ChecklistkEntry.FK_SERVIC,
            checklistContract.ChecklistkEntry.FK_TIPMOD,
            checklistContract.ChecklistkEntry.COLUMN_ALTA,
            checklistContract.ChecklistkEntry.COLUMN_MEDIA,
            checklistContract.ChecklistkEntry.COLUMN_BAIXA,
            checklistContract.ChecklistkEntry.COLUMN_SEQFAM

    };
    static final int COL_ETAPA_ID = 0;
    static final int COL_ETAPA_CODETAPA = 1;
    static final int COL_ETAPA_DESCETAPA = 2;

    static final int COL_USERLIST_ID = 0;
    static final int COL_USERLIST_USERNAME = 1;
    static final int COL_USERLIST_PASSWORD = 2;
    static final int COL_USERLIST_USERKEY = 3;

    static final int COL_CHECKLIST_ID = 0;
    static final int COL_CHECKLIST_ETAPA = 1;
    static final int COL_CHECKLIST_SERVIC = 2;
    static final int COL_CHECKLIST_TIPMOD = 3;
    static final int COL_CHECKLIST_ALTA = 4;
    static final int COL_CHECKLIST_MEDIA = 5;
    static final int COL_CHECKLIST_BEIXA = 6;
    static final int COL_CHECKLIST_SEQFAM = 7;


    String SelectEtapaWithFamiliaAndModeloFromChecklist;

    String SelectItemEtapaWithDateAndHour;

    String[] mSELECT_TTE_ARGUMENTS = {"", ""};
    String[] mSELECT_TTF_ARGUMENTS = {"", ""};
    String[] mSELECT_TTF_2_ARGUMENTS = {"", "",""};

    ListView listView;

    int CountPositions = 0;
    int PositionsCrontoller = 0;
    boolean reprovado = false;

    String result;
    String position;
    String spinner;
    String filepath;
    String encodedImage;
    String TextoDescri;
    String Data;
    String Hora;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ListView teste = new ListView(container.getContext());

        SharedPreferences pref = getContext().getSharedPreferences("InicioChecklist", MODE_PRIVATE);
        Data = pref.getString("Data", null);
        Hora = pref.getString("Hora", null);

        SelectItemEtapaWithDateAndHour = checklistContract.ItemChecklistEntry.COLUMN_data + " = ?" + " AND " + checklistContract.ItemChecklistEntry.COLUMN_hora + " = ?" + " AND " + checklistContract.ItemChecklistEntry.COLUMN_status + "= 'Reprovado'";

        mSELECT_TTF_ARGUMENTS[0] = Data;
        mSELECT_TTF_ARGUMENTS[1] = Hora;
        Cursor mCursorItems = getActivity().getContentResolver().query(
                BuscaEtapasItemChecklist, ItemChecklist_COLUMNS, SelectItemEtapaWithDateAndHour, mSELECT_TTF_ARGUMENTS, "_ID ASC");

        mCursorItems.moveToFirst();

        mChecklistAdapter = new chescklistAdapter(getActivity(), mCursorItems,0);

        listView = teste;
        listView.setAdapter(mChecklistAdapter);




        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, final View view,
                                    final int position, long id) {

                int Teste = position;
                new MaterialDialog.Builder(getActivity())
                        .title(R.string.RepeatItem)
                        .content(R.string.RepeatedItemText)
                        .positiveText(R.string.Dialog_OK)
                        .negativeText(R.string.cancelbutton)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                TextView textView = (TextView) view.findViewById(R.id.TVEtapa);
                                String text = textView.getText().toString().trim();
                                Intent intent = new Intent(getActivity(), ChecklistItemDetail.class);
                                intent.putExtra("Position", String.valueOf(position));
                                intent.putExtra("Etapa", text);
                                startActivityForResult(intent, 1);
                                PositionsCrontoller = PositionsCrontoller - 1;
                                reprovado = false;
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });

        return teste;

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri BuscaEtapasChecklist = checklistContract.ChecklistkEntry.buildChecklistUri();

        return new CursorLoader(getActivity(),BuscaEtapasChecklist, Checklist_COLUMNS, SelectEtapaWithFamiliaAndModeloFromChecklist, mSELECT_TTE_ARGUMENTS, "etapa ASC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mChecklistAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mChecklistAdapter.swapCursor(null);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        CountPositions = listView.getCount();
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                result = data.getStringExtra("result");
                position = data.getStringExtra("position");
                spinner = data.getStringExtra("spinner");
                filepath = data.getStringExtra("encodedImage");
                encodedImage = "";
                TextoDescri  = data.getStringExtra("TextoDescri");
                if (!Objects.equals(filepath, "")) {
                    Bitmap bm = BitmapFactory.decodeFile(filepath);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();
                    encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
                }

                updateView(position);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }

            Cursor mCursorItems = getActivity().getContentResolver().query(
                    BuscaEtapasItemChecklist, ItemChecklist_COLUMNS, SelectItemEtapaWithDateAndHour, mSELECT_TTF_ARGUMENTS, "_ID ASC");
            mCursorItems.moveToFirst();
            mChecklistAdapter = new chescklistAdapter(getActivity(), mCursorItems,0);
            listView.setAdapter(mChecklistAdapter);
        }

    }

    private void updateView(String descEtapa){
        if (Objects.equals(result, "true")) {

            ContentValues ItemChecklist = new ContentValues();
            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_Critic, spinner);
            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_Img, filepath);
            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_TextoComp, TextoDescri);
            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_status, "Aprovado");
            mSELECT_TTF_2_ARGUMENTS[0] = descEtapa;
            mSELECT_TTF_2_ARGUMENTS[1] = Data;
            mSELECT_TTF_2_ARGUMENTS[2] = Hora;
            getContext().getContentResolver().update(checklistContract.ItemChecklistEntry.CONTENT_URI, ItemChecklist,
                    checklistContract.ItemChecklistEntry.COLUMN_Desc + " = ?"+ " AND " + checklistContract.ItemChecklistEntry.COLUMN_data + " = ?" + " AND " + checklistContract.ItemChecklistEntry.COLUMN_hora + " = ?",mSELECT_TTF_2_ARGUMENTS);
        }else {
            reprovado = true;

            ContentValues ItemChecklist = new ContentValues();

            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_Critic, spinner);
            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_Img, filepath);
            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_TextoComp, TextoDescri);
            ItemChecklist.put(checklistContract.ItemChecklistEntry.COLUMN_status, "Reprovado");

            mSELECT_TTF_2_ARGUMENTS[0] = descEtapa;
            mSELECT_TTF_2_ARGUMENTS[1] = Data;
            mSELECT_TTF_2_ARGUMENTS[2] = Hora;
            getContext().getContentResolver().update(checklistContract.ItemChecklistEntry.CONTENT_URI, ItemChecklist,
                    checklistContract.ItemChecklistEntry.COLUMN_Desc + " = ?"+ " AND " + checklistContract.ItemChecklistEntry.COLUMN_data + " = ?" + " AND " + checklistContract.ItemChecklistEntry.COLUMN_hora + " = ?",mSELECT_TTF_2_ARGUMENTS);
        }
    }
}
